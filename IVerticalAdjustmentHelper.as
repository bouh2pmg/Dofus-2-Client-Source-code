package 
{
    import flashx.textLayout.compose.*;
    import flashx.textLayout.container.*;

    private interface IVerticalAdjustmentHelper
    {

        private function IVerticalAdjustmentHelper();

        function getBottom(param1:IVerticalJustificationLine, param2:ContainerController, param3:int, param4:int) : Number;

        function computeMiddleAdjustment(param1:Number) : Number;

        function applyAdjustment(param1:IVerticalJustificationLine) : void;

        function applyAdjustmentToFloat(param1:FloatCompositionData) : void;

        function computeBottomAdjustment(param1:Number) : Number;

        function computeJustifyAdjustment(param1:Array, param2:int, param3:int) : Number;

        function applyJustifyAdjustment(param1:Array, param2:int, param3:int) : void;

    }
}
