package 
{
    import com.ankamagames.tiphon.types.look.*;

    private class PlayerInfo extends Object
    {
        public var id:uint;
        public var name:String;
        public var look:TiphonEntityLook;
        public var skillLevel:int;

        private function PlayerInfo()
        {
            return;
        }// end function

    }
}
