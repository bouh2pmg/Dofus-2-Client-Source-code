package 
{
    import __AS3__.vec.*;

    private class Edge extends Object
    {
        private var _xbase:Number;
        private var _spanList:Vector.<Span>;

        private function Edge()
        {
            this._spanList = new Vector.<Span>;
            return;
        }// end function

        public function initialize(param1:Number) : void
        {
            this._xbase = param1;
            this._spanList.length = 0;
            return;
        }// end function

        public function get base() : Number
        {
            return this._xbase;
        }// end function

        public function addSpan(param1:Number, param2:Number, param3:Number) : void
        {
            this._spanList.push(new Span(param1, param2, param3));
            return;
        }// end function

        public function removeSpan(param1:Number, param2:Number, param3:Number) : void
        {
            var _loc_4:int;
            while (_loc_4 < this._spanList.length)
            {
                
                if (this._spanList[_loc_4].equals(param1, param2, param3))
                {
                    this._spanList.splice(_loc_4, 1);
                    return;
                }
                _loc_4++;
            }
            return;
        }// end function

        public function get numSpans() : int
        {
            return this._spanList.length;
        }// end function

        public function getMaxForSpan(param1:Number, param2:Number) : Number
        {
            var _loc_4:Span;
            var _loc_3:Number;
            for each (_loc_4 in this._spanList)
            {
                
                if (_loc_4.x > _loc_3 && _loc_4.overlapsInY(param1, param2))
                {
                    _loc_3 = _loc_4.x;
                }
            }
            return _loc_3;
        }// end function

        public function findNextTransition(param1:Number) : Number
        {
            var _loc_3:Span;
            var _loc_2:* = Number.MAX_VALUE;
            for each (_loc_3 in this._spanList)
            {
                
                if (_loc_3.ymin > param1 && _loc_3.ymin < _loc_2)
                {
                    _loc_2 = _loc_3.ymin;
                }
                if (_loc_3.ymax > param1 && _loc_3.ymax < _loc_2)
                {
                    _loc_2 = _loc_3.ymax;
                }
            }
            return _loc_2;
        }// end function

    }
}
