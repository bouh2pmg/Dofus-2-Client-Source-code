package 
{

    private class GlobalSWFContext extends Object implements ISWFContext
    {
        public static const globalSWFContext:GlobalSWFContext = new GlobalSWFContext;

        private function GlobalSWFContext()
        {
            return;
        }// end function

        public function callInContext(param1:Function, param2:Object, param3:Array, param4:Boolean = true)
        {
            if (param4)
            {
                return param1.apply(param2, param3);
            }
            param1.apply(param2, param3);
            return;
        }// end function

    }
}
