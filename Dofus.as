package 
{
    import Dofus.*;
    import com.ankamagames.atouin.*;
    import com.ankamagames.berilia.*;
    import com.ankamagames.berilia.components.*;
    import com.ankamagames.berilia.managers.*;
    import com.ankamagames.berilia.utils.web.*;
    import com.ankamagames.dofus.*;
    import com.ankamagames.dofus.console.moduleLogger.*;
    import com.ankamagames.dofus.kernel.*;
    import com.ankamagames.dofus.kernel.sound.*;
    import com.ankamagames.dofus.kernel.sound.manager.*;
    import com.ankamagames.dofus.logic.game.approach.managers.*;
    import com.ankamagames.dofus.logic.game.fight.miscs.*;
    import com.ankamagames.dofus.misc.*;
    import com.ankamagames.dofus.misc.interClient.*;
    import com.ankamagames.dofus.misc.utils.*;
    import com.ankamagames.dofus.misc.utils.errormanager.*;
    import com.ankamagames.dofus.network.enums.*;
    import com.ankamagames.dofus.network.types.updater.*;
    import com.ankamagames.dofus.types.*;
    import com.ankamagames.dofus.types.entities.*;
    import com.ankamagames.jerakine.logger.*;
    import com.ankamagames.jerakine.managers.*;
    import com.ankamagames.jerakine.protocolAudio.*;
    import com.ankamagames.jerakine.resources.adapters.impl.*;
    import com.ankamagames.jerakine.types.*;
    import com.ankamagames.jerakine.types.events.*;
    import com.ankamagames.jerakine.utils.crypto.*;
    import com.ankamagames.jerakine.utils.display.*;
    import com.ankamagames.jerakine.utils.files.*;
    import com.ankamagames.jerakine.utils.memory.*;
    import com.ankamagames.jerakine.utils.system.*;
    import com.ankamagames.tiphon.display.*;
    import com.ankamagames.tiphon.engine.*;
    import com.ankamagames.tiphon.events.*;
    import flash.desktop.*;
    import flash.display.*;
    import flash.events.*;
    import flash.filesystem.*;
    import flash.filters.*;
    import flash.system.*;
    import flash.text.*;
    import flash.utils.*;
    import flash.xml.*;

    public class Dofus extends Sprite implements IFramerateListener, IApplicationContainer
    {
        private var _uiContainer:Sprite;
        private var _worldContainer:Sprite;
        private var _fpsDisplay:TextField;
        private var _buildType:String;
        private var _instanceId:uint;
        private var _doOptions:DofusOptions;
        private var _invokeArguments:Array;
        private var _blockLoading:Boolean;
        private var _initialized:Boolean = false;
        private var _forcedLang:String;
        private var _displayState:String;
        public var REG_LOCAL_CONNECTION_ID:uint = 0;
        static const _log:Logger = Log.getLogger(getQualifiedClassName(this));
        private static var _self:Dofus;

        public function Dofus()
        {
            var stage:* = this.stage;
            if (!stage)
            {
                stage = loaderInfo.loader.stage;
                AirScanner.init(getQualifiedClassName(loaderInfo.loader.parent) == "DofusLoader");
            }
            else
            {
                AirScanner.init(false);
            }
            _self = this;
            var r:* = stage.stageWidth / stage.stageHeight;
            var clientDimentionSo:* = CustomSharedObject.getLocal("clientData");
            if (clientDimentionSo.data != null && clientDimentionSo.data.displayState == NativeWindowDisplayState.MAXIMIZED && Capabilities.os.substr(0, 3) == "Win" && stage.displayState != StageDisplayState["FULL_SCREEN_INTERACTIVE"])
            {
                stage.nativeWindow.maximize();
                this._displayState = NativeWindowDisplayState.MAXIMIZED;
            }
            if (Screen.mainScreen.bounds.width < Screen.mainScreen.bounds.height)
            {
                stage.stageWidth = Screen.mainScreen.bounds.width * 0.8;
                stage.stageHeight = stage.stageWidth / r;
            }
            else
            {
                stage.stageHeight = Screen.mainScreen.bounds.height * 0.8;
                stage.stageWidth = r * stage.stageHeight;
            }
            if (AirScanner.hasAir())
            {
                stage["nativeWindow"].x = (Screen.mainScreen.bounds.width - stage.stageWidth) / 2;
                stage["nativeWindow"].y = (Screen.mainScreen.bounds.height - stage.stageHeight) / 2;
                stage["nativeWindow"].visible = true;
            }
            else
            {
                clientDimentionSo.close();
                stage.showDefaultContextMenu = false;
                Security.allowDomain("*");
            }
            new DofusErrorHandler();
            if (AirScanner.hasAir())
            {
                this._blockLoading = name != "root1";
            }
            ErrorManager.registerLoaderInfo(loaderInfo);
            mouseEnabled = false;
            tabChildren = false;
            if (AirScanner.hasAir())
            {
                try
                {
                    new AppIdModifier();
                }
                catch (e:Error)
                {
                }
            }
            if (AirScanner.hasAir())
            {
                NativeApplication.nativeApplication.addEventListener(Event.EXITING, this.onExiting);
                NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, this.onCall);
                stage.nativeWindow.addEventListener(NativeWindowDisplayStateEvent.DISPLAY_STATE_CHANGING, this.onResize);
            }
            return;
        }// end function

        private function onCall(param1:InvokeEvent) : void
        {
            var file:File;
            var stream:FileStream;
            var content:String;
            var xml:XMLDocument;
            var gameNode:XMLNode;
            var upperVersion:String;
            var configNode:XMLNode;
            var versionSplitted:Array;
            var version:String;
            var part:ContentPart;
            var e:* = param1;
            var arguments:* = _loc_2;
            if (!this._initialized)
            {
                CommandLineArguments.getInstance().setArguments(e.arguments);
                this._invokeArguments = e.arguments;
                if (!this._invokeArguments)
                {
                    this._invokeArguments = [];
                }
                try
                {
                    file = new File(File.applicationDirectory.nativePath + File.separator + "uplauncherComponents.xml");
                    if (file.exists)
                    {
                        PartManager.getInstance().createEmptyPartList();
                        stream = new FileStream();
                        stream.open(file, FileMode.READ);
                        content = stream.readMultiByte(file.size, "utf-8");
                        xml = new XMLDocument();
                        xml.ignoreWhite = true;
                        xml.parseXML(content);
                        gameNode = xml.firstChild;
                        upperVersion;
                        var _loc_4:int;
                        var _loc_5:* = gameNode.childNodes;
                        while (_loc_5 in _loc_4)
                        {
                            
                            configNode = _loc_5[_loc_4];
                            version = configNode.attributes["version"];
                            part = new ContentPart();
                            part.id = configNode.attributes["name"];
                            part.state = version ? (PartStateEnum.PART_UP_TO_DATE) : (PartStateEnum.PART_NOT_INSTALLED);
                            PartManager.getInstance().updatePart(part);
                            if (version && upperVersion == null || version > upperVersion)
                            {
                                upperVersion = version;
                            }
                        }
                        versionSplitted = upperVersion.split(".");
                        if (versionSplitted.length >= 5)
                        {
                            BuildInfos.BUILD_VERSION = new Version(versionSplitted[0], versionSplitted[1], versionSplitted[2]);
                            BuildInfos.BUILD_VERSION.revision = versionSplitted[3];
                            BuildInfos.BUILD_REVISION = versionSplitted[3];
                            BuildInfos.BUILD_VERSION.patch = versionSplitted[4];
                            BuildInfos.BUILD_PATCH = versionSplitted[4];
                        }
                    }
                    else
                    {
                        file = new File(File.applicationDirectory.nativePath + File.separator + "games_base.xml");
                        if (file.exists)
                        {
                            stream = new FileStream();
                            stream.open(file, FileMode.READ);
                            content = stream.readMultiByte(file.size, "utf-8");
                            xml = new XMLDocument();
                            xml.ignoreWhite = true;
                            xml.parseXML(content);
                            gameNode = xml.firstChild.firstChild;
                            var _loc_4:int;
                            var _loc_5:* = gameNode.childNodes;
                            while (_loc_5 in _loc_4)
                            {
                                
                                configNode = _loc_5[_loc_4];
                                if (configNode.nodeName == "version")
                                {
                                    version = configNode.firstChild.nodeValue;
                                    version = version.split("_")[0];
                                    versionSplitted = version.split(".");
                                    if (versionSplitted.length >= 5)
                                    {
                                        BuildInfos.BUILD_VERSION = new Version(versionSplitted[0], versionSplitted[1], versionSplitted[2]);
                                        BuildInfos.BUILD_VERSION.revision = versionSplitted[3];
                                        BuildInfos.BUILD_REVISION = versionSplitted[3];
                                        BuildInfos.BUILD_VERSION.patch = versionSplitted[4];
                                        BuildInfos.BUILD_PATCH = versionSplitted[4];
                                    }
                                }
                            }
                        }
                        else
                        {
                            BuildInfos.BUILD_VERSION.revision = BuildInfos.BUILD_REVISION;
                            BuildInfos.BUILD_VERSION.patch = BuildInfos.BUILD_PATCH;
                        }
                    }
                }
                catch (e:Error)
                {
                    BuildInfos.BUILD_VERSION.revision = BuildInfos.BUILD_REVISION;
                    BuildInfos.BUILD_VERSION.patch = BuildInfos.BUILD_PATCH;
                }
                if (BuildInfos.BUILD_TYPE < BuildTypeEnum.INTERNAL)
                {
                    Uri.enableSecureURI();
                }
                if (this.stage)
                {
                    this.init(this.stage);
                }
                try
                {
                    file = new File(CustomSharedObject.getCustomSharedObjectDirectory() + File.separator + "path.d2p");
                    if (!file.exists)
                    {
                        stream = new FileStream();
                        stream.open(file, FileMode.WRITE);
                        stream.writeUTF(File.applicationDirectory.nativePath);
                        stream.close();
                    }
                }
                catch (e:Error)
                {
                }
                this._initialized = true;
            }
            return;
        }// end function

        private function onResize(param1:NativeWindowDisplayStateEvent) : void
        {
            this._displayState = param1.afterDisplayState;
            return;
        }// end function

        public function getUiContainer() : DisplayObjectContainer
        {
            return this._uiContainer;
        }// end function

        public function getWorldContainer() : DisplayObjectContainer
        {
            return this._worldContainer;
        }// end function

        public function get options() : DofusOptions
        {
            return this._doOptions;
        }// end function

        public function get invokeArgs() : Array
        {
            return this._invokeArguments;
        }// end function

        public function get instanceId() : uint
        {
            return this._instanceId;
        }// end function

        public function get forcedLang() : String
        {
            return this._forcedLang;
        }// end function

        public function setDisplayOptions(param1:DofusOptions) : void
        {
            this._doOptions = param1;
            this._doOptions.addEventListener(PropertyChangeEvent.PROPERTY_CHANGED, this.onOptionChange);
            this._doOptions.flashQuality = this._doOptions.flashQuality;
            this._doOptions.fullScreen = this._doOptions.fullScreen;
            return;
        }// end function

        public function init(param1:DisplayObject, param2:uint = 0, param3:String = null) : void
        {
            if (this._blockLoading)
            {
                throw new SecurityError("You cannot load Dofus.");
            }
            this._instanceId = param2;
            this._forcedLang = param3;
            var _loc_4:* = new Sprite();
            new Sprite().name = "catchMouseEventCtr";
            _loc_4.graphics.beginFill(0);
            _loc_4.graphics.drawRect(0, 0, StageShareManager.startWidth, StageShareManager.startHeight);
            _loc_4.graphics.endFill();
            addChild(_loc_4);
            var _loc_5:* = CustomSharedObject.getLocal("appVersion");
            if (!CustomSharedObject.getLocal("appVersion").data.lastBuildVersion || _loc_5.data.lastBuildVersion != BuildInfos.BUILD_REVISION && BuildInfos.BUILD_TYPE < BuildTypeEnum.INTERNAL)
            {
                this.clearCache(true);
            }
            _loc_5 = CustomSharedObject.getLocal("appVersion");
            _loc_5.data.lastBuildVersion = BuildInfos.BUILD_REVISION;
            _loc_5.flush();
            _loc_5.close();
            SignedFileAdapter.defaultSignatureKey = SignatureKey.fromByte(new Constants.SIGNATURE_KEY_DATA as ByteArray);
            this.initKernel(this.stage, param1);
            this.initWorld();
            this.initUi();
            if (BuildInfos.BUILD_TYPE > BuildTypeEnum.ALPHA)
            {
                this.initDebug();
            }
            if (AirScanner.hasAir())
            {
                stage["nativeWindow"].addEventListener(Event.CLOSE, this.onClosed);
            }
            TiphonEventsManager.addListener(Tiphon.getInstance(), TiphonEvent.EVT_EVENT);
            SoundManager.getInstance().manager = new RegSoundManager();
            (SoundManager.getInstance().manager as RegSoundManager).forceSoundsDebugMode = true;
            Atouin.getInstance().addListener(SoundManager.getInstance().manager);
            return;
        }// end function

        private function onExiting(param1:Event) : void
        {
            this.saveClientSize();
            if (WebServiceDataHandler.getInstance().quit())
            {
                param1.preventDefault();
                param1.stopPropagation();
                WebServiceDataHandler.getInstance().addEventListener(WebServiceDataHandler.ALL_DATA_SENT, this.quitHandler);
            }
            return;
        }// end function

        public function quit() : void
        {
            if (!WebServiceDataHandler.getInstance().quit())
            {
                this.quitHandler();
            }
            else
            {
                _log.trace("We have data to send to the webservice. waiting...");
                WebServiceDataHandler.getInstance().addEventListener(WebServiceDataHandler.ALL_DATA_SENT, this.quitHandler);
                WebServiceDataHandler.getInstance().sendWaitingException();
            }
            return;
        }// end function

        private function quitHandler(param1:Event = null) : void
        {
            if (param1 != null)
            {
                param1.currentTarget.removeEventListener(WebServiceDataHandler.ALL_DATA_SENT, this.quitHandler);
                _log.trace("Data sent. Good to go. Bye bye");
            }
            if (Constants.EVENT_MODE)
            {
                this.reboot();
            }
            else if (AirScanner.hasAir())
            {
                stage.nativeWindow.close();
                if (NativeApplication.nativeApplication.openedWindows.length == 0)
                {
                    NativeApplication.nativeApplication.exit(0);
                }
            }
            return;
        }// end function

        public function onClose() : void
        {
            RegConnectionManager.getInstance().send(ProtocolEnum.SAY_GOODBYE, RegConnectionManager.getInstance().socketClientID);
            InterClientManager.destroy();
            return;
        }// end function

        public function clearCache(param1:Boolean = false, param2:Boolean = false) : void
        {
            var soList:Array;
            var file:File;
            var fileName:String;
            var selective:* = param1;
            var reboot:* = param2;
            StoreDataManager.getInstance().reset();
            var soFolder:* = new File(CustomSharedObject.getCustomSharedObjectDirectory());
            if (soFolder && soFolder.exists)
            {
                CustomSharedObject.closeAll();
                soList = soFolder.getDirectoryListing();
                var _loc_4:int;
                var _loc_5:* = soList;
                do
                {
                    
                    file = _loc_5[_loc_4];
                    fileName = FileUtils.getFileStartName(file.name);
                    if (selective)
                    {
                        switch(true)
                        {
                            case fileName.indexOf("Module_") == 0:
                            case fileName == "dofus":
                            case fileName.indexOf("Dofus_") == 0:
                            case fileName == "atouin":
                            case fileName == "berilia":
                            case fileName == "chat":
                            case fileName == "tiphon":
                            case fileName == "tubul":
                            case fileName.indexOf("externalNotifications_") == 0:
                            case fileName == "Berilia_binds":
                            case fileName == "maps":
                            case fileName == "logs":
                            case fileName == "uid":
                            case fileName == "appVersion":
                            {
                                break;
                            }
                            default:
                            {
                                break;
                            }
                        }
                    }
                    try
                    {
                        if (file.isDirectory)
                        {
                            file.deleteDirectory(true);
                        }
                        else
                        {
                            file.deleteFile();
                        }
                    }
                    catch (e:Error)
                    {
                    }
                }while (_loc_5 in _loc_4)
            }
            if (reboot)
            {
                AppIdModifier.getInstance().invalideCache();
                this.reboot();
            }
            return;
        }// end function

        public function reboot() : void
        {
            this.saveClientSize();
            var _loc_1:* = Kernel.getWorker();
            if (_loc_1)
            {
                _loc_1.clear();
            }
            _log.fatal("REBOOT");
            if (AirScanner.hasAir())
            {
                var _loc_2:* = NativeApplication.nativeApplication;
                _loc_2.NativeApplication.nativeApplication["exit"](42);
            }
            else
            {
                throw new Error("Reboot not implemented with flash");
            }
            return;
        }// end function

        public function renameApp(param1:String) : void
        {
            _log.debug("rename : Dofus - " + param1);
            if (AirScanner.hasAir())
            {
                stage["nativeWindow"].title = param1;
            }
            return;
        }// end function

        private function initKernel(param1:Stage, param2:DisplayObject) : void
        {
            Kernel.getInstance().init(param1, param2);
            LangManager.getInstance().handler = Kernel.getWorker();
            FontManager.getInstance().handler = Kernel.getWorker();
            Berilia.getInstance().handler = Kernel.getWorker();
            LangManager.getInstance().lang = "frFr";
            return;
        }// end function

        private function initWorld() : void
        {
            if (this._worldContainer)
            {
                removeChild(this._worldContainer);
            }
            this._worldContainer = new Sprite();
            addChild(this._worldContainer);
            this._worldContainer.mouseEnabled = false;
            return;
        }// end function

        private function initUi() : void
        {
            if (this._uiContainer)
            {
                removeChild(this._uiContainer);
            }
            this._uiContainer = new Sprite();
            addChild(this._uiContainer);
            this._uiContainer.mouseEnabled = false;
            var _loc_1:* = BuildInfos.BUILD_TYPE == BuildTypeEnum.DEBUG;
            Berilia.getInstance().verboseException = _loc_1;
            Berilia.getInstance().init(this._uiContainer, _loc_1, BuildInfos.BUILD_REVISION, !_loc_1);
            if (AirScanner.isStreamingVersion())
            {
                Berilia.embedIcons.SLOT_DEFAULT_ICON = EmbedAssets.getBitmap("DefaultBeriliaSlotIcon").bitmapData;
            }
            var _loc_2:* = new Uri("SharedDefinitions.swf");
            _loc_2.loaderContext = new LoaderContext(false, new ApplicationDomain());
            UiModuleManager.getInstance().sharedDefinitionContainer = _loc_2;
            EntityDisplayer.animationModifier = new CustomAnimStatiqueAnimationModifier();
            EntityDisplayer.setSubEntityDefaultBehavior(SubEntityBindingPointCategoryEnum.HOOK_POINT_CATEGORY_PET, new AnimStatiqueSubEntityBehavior());
            EntityDisplayer.setSubEntityDefaultBehavior(SubEntityBindingPointCategoryEnum.HOOK_POINT_CATEGORY_MOUNT_DRIVER, new RiderBehavior());
            CharacterWheel.animationModifier = new CustomAnimStatiqueAnimationModifier();
            CharacterWheel.setSubEntityDefaultBehavior(SubEntityBindingPointCategoryEnum.HOOK_POINT_CATEGORY_PET, new AnimStatiqueSubEntityBehavior());
            CharacterWheel.setSubEntityDefaultBehavior(SubEntityBindingPointCategoryEnum.HOOK_POINT_CATEGORY_MOUNT_DRIVER, new RiderBehavior());
            EntityDisplayer.lookAdaptater = EntityLookAdapter.tiphonizeLook;
            return;
        }// end function

        private function initDebug() : void
        {
            FramerateCounter.refreshRate = 250;
            FramerateCounter.addListener(this);
            this._buildType = BuildTypeParser.getTypeName(BuildInfos.BUILD_TYPE);
            this._fpsDisplay = new TextField();
            this._fpsDisplay.mouseEnabled = false;
            this._fpsDisplay.selectable = false;
            this._fpsDisplay.defaultTextFormat = new TextFormat("Verdana", 12, 16777215, true, false, false, null, null, TextFormatAlign.RIGHT);
            this._fpsDisplay.text = this._buildType + "\nr" + BuildInfos.BUILD_REVISION;
            this._fpsDisplay.filters = [new DropShadowFilter(0, 0, 0, 1, 2, 2, 5, 3)];
            this._fpsDisplay.width = 300;
            this._fpsDisplay.x = 1280 - this._fpsDisplay.width;
            addChild(this._fpsDisplay);
            if (Constants.EVENT_MODE)
            {
                this._fpsDisplay.visible = false;
            }
            return;
        }// end function

        public function toggleFPS() : void
        {
            this._fpsDisplay.visible = !this._fpsDisplay.visible;
            return;
        }// end function

        private function onClosed(param1:Event) : void
        {
            Console.getInstance().close();
            HttpServer.getInstance().close();
            return;
        }// end function

        private function onOptionChange(param1:PropertyChangeEvent) : void
        {
            if (param1.propertyName == "flashQuality")
            {
                if (param1.propertyValue == 0)
                {
                    StageShareManager.stage.quality = StageQuality.LOW;
                }
                else if (param1.propertyValue == 1)
                {
                    StageShareManager.stage.quality = StageQuality.MEDIUM;
                }
                else if (param1.propertyValue == 2)
                {
                    StageShareManager.stage.quality = StageQuality.HIGH;
                }
            }
            if (param1.propertyName == "fullScreen")
            {
                StageShareManager.setFullScreen(param1.propertyValue, false);
            }
            return;
        }// end function

        public function onFps(param1:uint) : void
        {
            var _loc_2:Object;
            if (this._fpsDisplay.visible)
            {
                _loc_2 = RasterizedAnimation.countFrames();
                this._fpsDisplay.htmlText = "<font color=\'#FFFFFF\'>" + param1 + " fps - " + this._buildType + "\n<font color=\'#B9B6ED\'>" + Memory.humanReadableUsage() + " - r" + BuildInfos.BUILD_REVISION + "\n<font color=\'#92D5D8\'> Anim/Img en cache - " + _loc_2.animations + "/" + _loc_2.frames;
            }
            return;
        }// end function

        private function saveClientSize() : void
        {
            var _loc_1:* = CustomSharedObject.getLocal("clientData");
            _loc_1.data.displayState = this._displayState;
            _loc_1.flush();
            _loc_1.close();
            return;
        }// end function

        public static function getInstance() : Dofus
        {
            return _self;
        }// end function

    }
}
