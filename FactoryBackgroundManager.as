package 
{
    import flashx.textLayout.compose.*;
    import flashx.textLayout.elements.*;

    private class FactoryBackgroundManager extends BackgroundManager
    {

        private function FactoryBackgroundManager()
        {
            return;
        }// end function

        override public function finalizeLine(param1:TextFlowLine) : void
        {
            var _loc_4:Object;
            var _loc_2:* = param1.getTextLine();
            var _loc_3:* = _lineDict[_loc_2];
            if (_loc_3)
            {
                _loc_4 = _loc_3[0];
                if (_loc_4)
                {
                    _loc_4.columnRect = param1.controller.columnState.getColumnAt(param1.columnIndex);
                }
            }
            return;
        }// end function

    }
}
