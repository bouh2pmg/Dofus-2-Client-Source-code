package 
{
    import flashx.textLayout.compose.*;
    import flashx.textLayout.container.*;
    import flashx.textLayout.formats.*;

    private class RL_VJHelper extends Object implements IVerticalAdjustmentHelper
    {
        private var _textFrame:ContainerController;
        private var adj:Number = 0;

        private function RL_VJHelper(param1:ContainerController) : void
        {
            this._textFrame = param1;
            return;
        }// end function

        public function getBottom(param1:IVerticalJustificationLine, param2:ContainerController, param3:int, param4:int) : Number
        {
            var _loc_8:FloatCompositionData;
            var _loc_5:* = this._textFrame.compositionWidth - Number(this._textFrame.getTotalPaddingLeft());
            var _loc_6:* = this._textFrame.compositionWidth - Number(this._textFrame.getTotalPaddingLeft()) + param1.x - param1.descent;
            var _loc_7:* = param3;
            while (_loc_7 < param4)
            {
                
                _loc_8 = param2.getFloatAt(_loc_7);
                if (_loc_8.floatType != Float.NONE)
                {
                    _loc_6 = Math.min(_loc_6, _loc_8.x + _loc_5);
                }
                _loc_7++;
            }
            return _loc_6;
        }// end function

        public function computeMiddleAdjustment(param1:Number) : Number
        {
            this.adj = param1 / 2;
            if (this.adj < 0)
            {
                this.adj = 0;
            }
            return -this.adj;
        }// end function

        public function computeBottomAdjustment(param1:Number) : Number
        {
            this.adj = param1;
            if (this.adj < 0)
            {
                this.adj = 0;
            }
            return -this.adj;
        }// end function

        public function applyAdjustment(param1:IVerticalJustificationLine) : void
        {
            param1.x = param1.x - this.adj;
            return;
        }// end function

        public function applyAdjustmentToFloat(param1:FloatCompositionData) : void
        {
            param1.x = param1.x - this.adj;
            return;
        }// end function

        public function computeJustifyAdjustment(param1:Array, param2:int, param3:int) : Number
        {
            this.adj = 0;
            if (param3 == 1)
            {
                return 0;
            }
            var _loc_4:* = param1[param2];
            var _loc_5:* = param1[param2].x;
            var _loc_6:* = param1[(param2 + param3)--];
            var _loc_7:* = Number(this._textFrame.getTotalPaddingLeft()) - this._textFrame.compositionWidth;
            var _loc_8:* = _loc_6.x - _loc_6.descent - _loc_7;
            if (_loc_6.x - _loc_6.descent - _loc_7 < 0)
            {
                return 0;
            }
            var _loc_9:* = _loc_6.x;
            this.adj = _loc_8 / (_loc_5 - _loc_9);
            return -this.adj;
        }// end function

        public function applyJustifyAdjustment(param1:Array, param2:int, param3:int) : void
        {
            var _loc_7:IVerticalJustificationLine;
            var _loc_8:Number;
            var _loc_9:Number;
            if (param3 == 1 || this.adj == 0)
            {
                return;
            }
            var _loc_4:* = param1[param2];
            var _loc_5:* = param1[param2].x;
            var _loc_6:* = param1[param2].x;
            var _loc_10:int;
            while (_loc_10 < param3)
            {
                
                _loc_7 = param1[_loc_10 + param2];
                _loc_9 = _loc_7.x;
                _loc_8 = _loc_5 - (_loc_6 - _loc_9) * (1 + this.adj);
                _loc_7.x = _loc_8;
                _loc_6 = _loc_9;
                _loc_5 = _loc_8;
                _loc_10++;
            }
            return;
        }// end function

    }
}
