package 
{
    import flashx.textLayout.elements.*;
    import flashx.textLayout.formats.*;

    private class HostFormatHelper extends Object
    {
        private var _format:ITextLayoutFormat;
        private var _computedPrototypeFormat:TextLayoutFormat;

        private function HostFormatHelper()
        {
            return;
        }// end function

        public function get format() : ITextLayoutFormat
        {
            return this._format;
        }// end function

        public function set format(param1:ITextLayoutFormat) : void
        {
            this._format = param1;
            this._computedPrototypeFormat = null;
            return;
        }// end function

        public function getComputedPrototypeFormat() : TextLayoutFormat
        {
            var _loc_1:ITextLayoutFormat;
            if (this._computedPrototypeFormat == null)
            {
                if (this._format is TextLayoutFormat || this._format is TextLayoutFormat)
                {
                    _loc_1 = this._format;
                }
                else
                {
                    _loc_1 = new TextLayoutFormat(this._format);
                }
                this._computedPrototypeFormat = FlowElement.createTextLayoutFormatPrototype(_loc_1, null);
            }
            return this._computedPrototypeFormat;
        }// end function

    }
}
