package 
{

    private class GIDObjectData extends Object
    {
        public var objects:Array;
        public var GIDObject:uint;

        private function GIDObjectData(param1:uint, param2:Array)
        {
            this.objects = param2;
            this.GIDObject = param1;
            return;
        }// end function

    }
}
