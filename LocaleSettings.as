package 
{
    import flashx.textLayout.formats.*;

    private class LocaleSettings extends Object
    {
        private var _justificationRule:String = null;
        private var _justificationStyle:String = null;
        private var _leadingModel:String = null;
        private var _dominantBaseline:String = null;

        private function LocaleSettings()
        {
            return;
        }// end function

        public function get justificationRule() : String
        {
            return this._justificationRule;
        }// end function

        public function set justificationRule(param1:String) : void
        {
            var _loc_2:* = TextLayoutFormat.justificationRuleProperty.setHelper(this._justificationRule, param1);
            this._justificationRule = _loc_2 == null ? (null) : (_loc_2 as String);
            return;
        }// end function

        public function get justificationStyle() : String
        {
            return this._justificationStyle;
        }// end function

        public function set justificationStyle(param1:String) : void
        {
            var _loc_2:* = TextLayoutFormat.justificationStyleProperty.setHelper(this._justificationStyle, param1);
            this._justificationStyle = _loc_2 == null ? (null) : (_loc_2 as String);
            return;
        }// end function

        public function get leadingModel() : String
        {
            return this._leadingModel;
        }// end function

        public function set leadingModel(param1:String) : void
        {
            var _loc_2:* = TextLayoutFormat.leadingModelProperty.setHelper(this._leadingModel, param1);
            this._leadingModel = _loc_2 == null ? (null) : (_loc_2 as String);
            return;
        }// end function

        public function get dominantBaseline() : String
        {
            return this._dominantBaseline;
        }// end function

        public function set dominantBaseline(param1:String) : void
        {
            var _loc_2:* = TextLayoutFormat.dominantBaselineProperty.setHelper(this._dominantBaseline, param1);
            this._dominantBaseline = _loc_2 == null ? (null) : (_loc_2 as String);
            return;
        }// end function

    }
}
