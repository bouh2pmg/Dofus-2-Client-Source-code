package 
{
    import flash.text.engine.*;
    import flashx.textLayout.compose.*;

    private class AlignData extends Object
    {
        public var textFlowLine:TextFlowLine;
        public var textLine:TextLine;
        public var lineWidth:Number;
        public var textAlign:String;
        public var leftSideGap:Number;
        public var rightSideGap:Number;
        public var textIndent:Number;

        private function AlignData(param1:TextFlowLine)
        {
            this.textFlowLine = param1;
            return;
        }// end function

    }
}
