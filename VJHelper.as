package 
{
    import flash.text.engine.*;

    private class VJHelper extends Object implements IVerticalJustificationLine
    {
        private var _line:TextLine;
        private var _height:Number;

        private function VJHelper(param1:TextLine, param2:Number)
        {
            this._line = param1;
            this._height = param2;
            return;
        }// end function

        public function get x() : Number
        {
            return this._line.x;
        }// end function

        public function set x(param1:Number) : void
        {
            this._line.x = param1;
            return;
        }// end function

        public function get y() : Number
        {
            return this._line.y;
        }// end function

        public function set y(param1:Number) : void
        {
            this._line.y = param1;
            return;
        }// end function

        public function get ascent() : Number
        {
            return this._line.ascent;
        }// end function

        public function get descent() : Number
        {
            return this._line.descent;
        }// end function

        public function get height() : Number
        {
            return this._height;
        }// end function

    }
}
