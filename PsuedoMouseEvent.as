package 
{
    import flash.display.*;
    import flash.events.*;

    private class PsuedoMouseEvent extends MouseEvent
    {

        private function PsuedoMouseEvent(param1:String, param2:Boolean = true, param3:Boolean = false, param4:Number = 1.#QNAN, param5:Number = 1.#QNAN, param6:InteractiveObject = null, param7:Boolean = false, param8:Boolean = false, param9:Boolean = false, param10:Boolean = false)
        {
            super(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10);
            return;
        }// end function

        override public function get currentTarget() : Object
        {
            return relatedObject;
        }// end function

        override public function get target() : Object
        {
            return relatedObject;
        }// end function

    }
}
