package com.ankamagames.berilia.utils.web
{
    import com.ankamagames.jerakine.utils.errors.*;
    import flash.events.*;
    import flash.filesystem.*;
    import flash.utils.*;

    public class HttpServer extends EventDispatcher
    {
        private var _server:Object;
        private var _sockets:Array;
        private var _usedPort:uint;
        private var _rootPath:String;
        private static var _self:HttpServer;

        public function HttpServer()
        {
            if (_self)
            {
                throw new SingletonError();
            }
            return;
        }// end function

        public function get rootPath() : String
        {
            return this._rootPath;
        }// end function

        public function init(param1:File) : Boolean
        {
            var rootPath:* = param1;
            this._rootPath = rootPath.nativePath;
            if (this._usedPort)
            {
                return true;
            }
            this._sockets = new Array();
            this._server = new getDefinitionByName("flash.net.ServerSocket") as Class;
            this._server.addEventListener(Event.CONNECT, this.onConnect);
            var tryCount:uint;
            var currentPort:uint;
            do
            {
                
                try
                {
                    this._server.bind(currentPort, "127.0.0.1");
                    this._server.listen();
                    this._usedPort = currentPort;
                    trace("Listening on port " + currentPort + "...\n");
                    return true;
                }
                catch (e:Error)
                {
                    currentPort = currentPort++;
                    tryCount = tryCount--;
                }
            }while (tryCount)
            return false;
        }// end function

        public function getUrlTo(param1:String) : String
        {
            return "http://localhost:" + this._usedPort + "/" + param1;
        }// end function

        public function close() : void
        {
            var _loc_1:HttpSocket;
            for each (_loc_1 in this._sockets)
            {
                
                _loc_1.tearDown();
            }
            if (this._server != null && this._server.bound)
            {
                this._server.removeEventListener(Event.CONNECT, this.onConnect);
                this._server.close();
                trace("Server closed");
            }
            return;
        }// end function

        private function onConnect(param1:Event) : void
        {
            var _loc_2:* = new HttpSocket(Object(param1).socket, this._rootPath);
            _loc_2.addEventListener(Event.COMPLETE, this.onHttpSocketComplete);
            this._sockets.push(_loc_2);
            return;
        }// end function

        private function onHttpSocketComplete(param1:Event) : void
        {
            var _loc_2:* = param1.target as HttpSocket;
            this._sockets.splice(this._sockets.indexOf(_loc_2), 1);
            return;
        }// end function

        public static function getInstance() : HttpServer
        {
            if (!_self)
            {
                _self = new HttpServer;
            }
            return _self;
        }// end function

    }
}
