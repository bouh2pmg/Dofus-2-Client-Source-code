package com.ankamagames.berilia
{

    public interface UIComponent extends MessageHandler
    {

        public function UIComponent();

        function remove() : void;

    }
}
