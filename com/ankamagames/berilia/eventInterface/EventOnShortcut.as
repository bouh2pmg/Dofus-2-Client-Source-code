package com.ankamagames.berilia.eventInterface
{

    public interface EventOnShortcut extends UIEvent
    {

        public function EventOnShortcut();

        function eventOnShortcut(param1:String) : Boolean;

    }
}
