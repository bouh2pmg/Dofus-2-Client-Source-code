package com.ankamagames.jerakine.sequencer
{

    public interface ISubSequenceSequencable extends ISequencable, IEventDispatcher
    {

        public function ISubSequenceSequencable();

    }
}
