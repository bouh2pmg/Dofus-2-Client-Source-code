package com.ankamagames.jerakine.data
{
    import com.ankamagames.jerakine.*;
    import com.ankamagames.jerakine.managers.*;
    import com.ankamagames.jerakine.resources.events.*;
    import com.ankamagames.jerakine.types.events.*;
    import com.ankamagames.jerakine.utils.errors.*;

    public class GameDataUpdater extends DataUpdateManager
    {
        private static var _self:GameDataUpdater;

        public function GameDataUpdater()
        {
            if (_self)
            {
                throw new SingletonError();
            }
            return;
        }// end function

        override protected function checkFileVersion(param1:String, param2:String) : Boolean
        {
            return false;
        }// end function

        override public function clear() : void
        {
            GameDataFileAccessor.getInstance().close();
            return;
        }// end function

        override protected function onLoaded(param1:ResourceLoadedEvent) : void
        {
            switch(param1.uri.fileType)
            {
                case "d2o":
                case "d2os":
                {
                    GameDataFileAccessor.getInstance().init(param1.uri);
                    _versions[param1.uri.tag.file] = param1.uri.tag.version;
                    StoreDataManager.getInstance().setData(JerakineConstants.DATASTORE_FILES_INFO, _storeKey, _versions);
                    dispatchEvent(new LangFileEvent(LangFileEvent.COMPLETE, false, false, param1.uri.tag.file));
                    _dataFilesLoaded = true;
                    _loadedFileCount++;
                    break;
                }
                default:
                {
                    super.onLoaded(param1);
                    break;
                    break;
                }
            }
            return;
        }// end function

        public static function getInstance() : GameDataUpdater
        {
            if (!_self)
            {
                _self = new GameDataUpdater;
            }
            return _self;
        }// end function

    }
}
