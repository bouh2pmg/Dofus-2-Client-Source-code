package com.ankamagames.jerakine.pools
{
    import com.ankamagames.jerakine.logger.*;
    import flash.display.*;
    import flash.utils.*;

    public class PoolableLoader extends Loader implements Poolable
    {
        static const _log:Logger = Log.getLogger(getQualifiedClassName(this));

        public function PoolableLoader()
        {
            return;
        }// end function

        public function free() : void
        {
            unload();
            return;
        }// end function

    }
}
