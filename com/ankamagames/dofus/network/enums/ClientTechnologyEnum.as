package com.ankamagames.dofus.network.enums
{

    public class ClientTechnologyEnum extends Object
    {
        public static const CLIENT_TECHNOLOGY_UNKNOWN:uint = 0;
        public static const CLIENT_AIR:uint = 1;
        public static const CLIENT_FLASH:uint = 2;

        public function ClientTechnologyEnum()
        {
            return;
        }// end function

    }
}
