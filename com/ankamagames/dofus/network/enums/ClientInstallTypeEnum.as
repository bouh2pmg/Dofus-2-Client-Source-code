package com.ankamagames.dofus.network.enums
{

    public class ClientInstallTypeEnum extends Object
    {
        public static const CLIENT_INSTALL_UNKNOWN:uint = 0;
        public static const CLIENT_BUNDLE:uint = 1;
        public static const CLIENT_STREAMING:uint = 2;

        public function ClientInstallTypeEnum()
        {
            return;
        }// end function

    }
}
