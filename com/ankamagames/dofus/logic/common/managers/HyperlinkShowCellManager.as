package com.ankamagames.dofus.logic.common.managers
{
    import com.ankamagames.dofus.types.sequences.*;
    import com.ankamagames.jerakine.sequencer.*;

    public class HyperlinkShowCellManager extends Object
    {

        public function HyperlinkShowCellManager()
        {
            return;
        }// end function

        public static function showCell(... args) : void
        {
            var sq:SerialSequencer;
            var args:* = args;
            try
            {
                sq = new SerialSequencer();
                sq.addStep(new AddGfxEntityStep(645, args[int(Math.random() * args.length)]));
                sq.start();
            }
            catch (e:Error)
            {
            }
            return;
        }// end function

    }
}
