package com.ankamagames.dofus.logic.game.roleplay.types
{
    import __AS3__.vec.*;
    import com.ankamagames.dofus.datacenter.monsters.*;
    import com.ankamagames.dofus.datacenter.npcs.*;
    import com.ankamagames.dofus.network.types.game.context.fight.*;

    public class RoleplayTeamFightersTooltipInformation extends Object
    {
        public var fighters:Vector.<Fighter>;

        public function RoleplayTeamFightersTooltipInformation(param1:FightTeam)
        {
            var _loc_2:FightTeamMemberInformations;
            var _loc_3:Fighter;
            var _loc_4:Monster;
            var _loc_5:uint;
            var _loc_6:String;
            var _loc_7:uint;
            var _loc_8:String;
            var _loc_9:String;
            var _loc_10:String;
            this.fighters = new Vector.<Fighter>;
            for each (_loc_2 in param1.teamInfos.teamMembers)
            {
                
                switch(true)
                {
                    case _loc_2 is FightTeamMemberCharacterInformations:
                    {
                        _loc_3 = new Fighter((_loc_2 as FightTeamMemberCharacterInformations).name, (_loc_2 as FightTeamMemberCharacterInformations).level);
                        break;
                    }
                    case _loc_2 is FightTeamMemberMonsterInformations:
                    {
                        _loc_4 = Monster.getMonsterById((_loc_2 as FightTeamMemberMonsterInformations).monsterId);
                        _loc_5 = _loc_4.getMonsterGrade((_loc_2 as FightTeamMemberMonsterInformations).grade).level;
                        _loc_6 = _loc_4.name;
                        _loc_3 = new Fighter(_loc_6, _loc_5);
                        break;
                    }
                    case _loc_2 is FightTeamMemberTaxCollectorInformations:
                    {
                        _loc_7 = (_loc_2 as FightTeamMemberTaxCollectorInformations).level;
                        _loc_8 = TaxCollectorFirstname.getTaxCollectorFirstnameById((_loc_2 as FightTeamMemberTaxCollectorInformations).firstNameId).firstname;
                        _loc_9 = TaxCollectorName.getTaxCollectorNameById((_loc_2 as FightTeamMemberTaxCollectorInformations).lastNameId).name;
                        _loc_10 = _loc_8 + " " + _loc_9;
                        _loc_3 = new Fighter(_loc_10, _loc_7);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                this.fighters.push(_loc_3);
            }
            return;
        }// end function

    }
}
