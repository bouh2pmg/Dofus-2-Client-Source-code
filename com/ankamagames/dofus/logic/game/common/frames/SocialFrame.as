package com.ankamagames.dofus.logic.game.common.frames
{
    import __AS3__.vec.*;
    import com.ankamagames.berilia.managers.*;
    import com.ankamagames.dofus.datacenter.npcs.*;
    import com.ankamagames.dofus.externalnotification.enums.*;
    import com.ankamagames.dofus.internalDatacenter.guild.*;
    import com.ankamagames.dofus.internalDatacenter.people.*;
    import com.ankamagames.dofus.internalDatacenter.world.*;
    import com.ankamagames.dofus.kernel.*;
    import com.ankamagames.dofus.kernel.net.*;
    import com.ankamagames.dofus.logic.common.managers.*;
    import com.ankamagames.dofus.logic.game.common.actions.*;
    import com.ankamagames.dofus.logic.game.common.actions.guild.*;
    import com.ankamagames.dofus.logic.game.common.actions.social.*;
    import com.ankamagames.dofus.logic.game.common.actions.taxCollector.*;
    import com.ankamagames.dofus.logic.game.common.managers.*;
    import com.ankamagames.dofus.misc.*;
    import com.ankamagames.dofus.misc.lists.*;
    import com.ankamagames.dofus.network.enums.*;
    import com.ankamagames.dofus.network.messages.game.chat.report.*;
    import com.ankamagames.dofus.network.messages.game.context.roleplay.npc.*;
    import com.ankamagames.dofus.network.messages.game.friend.*;
    import com.ankamagames.dofus.network.messages.game.guild.*;
    import com.ankamagames.dofus.network.messages.game.guild.tax.*;
    import com.ankamagames.dofus.network.messages.game.report.*;
    import com.ankamagames.dofus.network.messages.game.social.*;
    import com.ankamagames.dofus.network.types.game.friend.*;
    import com.ankamagames.dofus.network.types.game.guild.*;
    import com.ankamagames.dofus.network.types.game.paddock.*;
    import com.ankamagames.jerakine.data.*;
    import com.ankamagames.jerakine.logger.*;
    import com.ankamagames.jerakine.messages.*;
    import com.ankamagames.jerakine.types.enums.*;
    import com.ankamagames.jerakine.utils.system.*;
    import d2network.*;
    import flash.utils.*;

    public class SocialFrame extends Object implements Frame
    {
        private var _guildDialogFrame:GuildDialogFrame;
        private var _friendsList:Array;
        private var _enemiesList:Array;
        private var _ignoredList:Array;
        private var _spouse:SpouseWrapper;
        private var _hasGuild:Boolean = false;
        private var _hasSpouse:Boolean = false;
        private var _guild:GuildWrapper;
        private var _guildMembers:Vector.<GuildMember>;
        private var _guildHouses:Vector.<GuildHouseWrapper>;
        private var _guildHousesList:Boolean = false;
        private var _guildHousesListUpdate:Boolean = false;
        private var _guildPaddocks:Vector.<PaddockContentInformations>;
        private var _guildPaddocksMax:int = 1;
        private var _upGuildEmblem:EmblemWrapper;
        private var _backGuildEmblem:EmblemWrapper;
        private var _warnOnFrienConnec:Boolean;
        private var _warnOnMemberConnec:Boolean;
        private var _warnWhenFriendOrGuildMemberLvlUp:Boolean;
        private var _autoLeaveHelpers:Boolean;
        static const _log:Logger = Log.getLogger(getQualifiedClassName(this));

        public function SocialFrame()
        {
            this._guildHouses = new Vector.<GuildHouseWrapper>;
            this._guildPaddocks = new Vector.<PaddockContentInformations>;
            return;
        }// end function

        public function get priority() : int
        {
            return Priority.NORMAL;
        }// end function

        public function get friendsList() : Array
        {
            return this._friendsList;
        }// end function

        public function get enemiesList() : Array
        {
            return this._enemiesList;
        }// end function

        public function get ignoredList() : Array
        {
            return this._ignoredList;
        }// end function

        public function get spouse() : SpouseWrapper
        {
            return this._spouse;
        }// end function

        public function get hasGuild() : Boolean
        {
            return this._hasGuild;
        }// end function

        public function get hasSpouse() : Boolean
        {
            return this._hasSpouse;
        }// end function

        public function get guild() : GuildWrapper
        {
            return this._guild;
        }// end function

        public function get guildmembers() : Vector.<GuildMember>
        {
            return this._guildMembers;
        }// end function

        public function get guildHouses() : Vector.<GuildHouseWrapper>
        {
            return this._guildHouses;
        }// end function

        public function get guildPaddocks() : Vector.<PaddockContentInformations>
        {
            return this._guildPaddocks;
        }// end function

        public function get maxGuildPaddocks() : int
        {
            return this._guildPaddocksMax;
        }// end function

        public function get warnFriendConnec() : Boolean
        {
            return this._warnOnFrienConnec;
        }// end function

        public function get warnMemberConnec() : Boolean
        {
            return this._warnOnMemberConnec;
        }// end function

        public function get warnWhenFriendOrGuildMemberLvlUp() : Boolean
        {
            return this._warnWhenFriendOrGuildMemberLvlUp;
        }// end function

        public function pushed() : Boolean
        {
            this._friendsList = new Array();
            this._enemiesList = new Array();
            this._ignoredList = new Array();
            this._guildDialogFrame = new GuildDialogFrame();
            ConnectionsHandler.getConnection().send(new FriendsGetListMessage());
            ConnectionsHandler.getConnection().send(new IgnoredGetListMessage());
            return true;
        }// end function

        public function process(param1:Message) : Boolean
        {
            var _loc_2:GuildMembershipMessage;
            var _loc_3:FriendsListMessage;
            var _loc_4:FriendsListWithSpouseMessage;
            var _loc_5:SpouseWrapper;
            var _loc_6:IgnoredListMessage;
            var _loc_7:OpenSocialAction;
            var _loc_8:FriendsListRequestAction;
            var _loc_9:EnemiesListRequestAction;
            var _loc_10:SpouseRequestAction;
            var _loc_11:AddFriendAction;
            var _loc_12:FriendAddedMessage;
            var _loc_13:FriendWrapper;
            var _loc_14:FriendAddFailureMessage;
            var _loc_15:String;
            var _loc_16:AddEnemyAction;
            var _loc_17:IgnoredAddedMessage;
            var _loc_18:IgnoredAddFailureMessage;
            var _loc_19:String;
            var _loc_20:RemoveFriendAction;
            var _loc_21:FriendDeleteRequestMessage;
            var _loc_22:FriendDeleteResultMessage;
            var _loc_23:String;
            var _loc_24:FriendUpdateMessage;
            var _loc_25:FriendWrapper;
            var _loc_26:RemoveEnemyAction;
            var _loc_27:IgnoredDeleteRequestMessage;
            var _loc_28:IgnoredDeleteResultMessage;
            var _loc_29:AddIgnoredAction;
            var _loc_30:RemoveIgnoredAction;
            var _loc_31:IgnoredDeleteRequestMessage;
            var _loc_32:JoinFriendAction;
            var _loc_33:FriendJoinRequestMessage;
            var _loc_34:JoinSpouseAction;
            var _loc_35:FriendSpouseFollowAction;
            var _loc_36:FriendSpouseFollowWithCompassRequestMessage;
            var _loc_37:FriendWarningSetAction;
            var _loc_38:FriendSetWarnOnConnectionMessage;
            var _loc_39:MemberWarningSetAction;
            var _loc_40:GuildMemberSetWarnOnConnectionMessage;
            var _loc_41:FriendOrGuildMemberLevelUpWarningSetAction;
            var _loc_42:FriendSetWarnOnLevelGainMessage;
            var _loc_43:SpouseStatusMessage;
            var _loc_44:FriendWarnOnConnectionStateMessage;
            var _loc_45:GuildMemberWarnOnConnectionStateMessage;
            var _loc_46:FriendWarnOnLevelGainStateMessage;
            var _loc_47:GuildInformationsMembersMessage;
            var _loc_48:GuildHousesInformationMessage;
            var _loc_49:GuildModificationStartedMessage;
            var _loc_50:GuildCreationResultMessage;
            var _loc_51:String;
            var _loc_52:GuildInvitedMessage;
            var _loc_53:GuildInvitationStateRecruterMessage;
            var _loc_54:GuildInvitationStateRecrutedMessage;
            var _loc_55:GuildJoinedMessage;
            var _loc_56:String;
            var _loc_57:GuildUIOpenedMessage;
            var _loc_58:GuildInformationsGeneralMessage;
            var _loc_59:GuildInformationsMemberUpdateMessage;
            var _loc_60:GuildMember;
            var _loc_61:GuildMemberLeavingMessage;
            var _loc_62:uint;
            var _loc_63:GuildInfosUpgradeMessage;
            var _loc_64:GuildFightPlayersHelpersJoinMessage;
            var _loc_65:GuildFightPlayersHelpersLeaveMessage;
            var _loc_66:GuildFightPlayersEnemiesListMessage;
            var _loc_67:GuildFightPlayersEnemyRemoveMessage;
            var _loc_68:TaxCollectorMovementMessage;
            var _loc_69:String;
            var _loc_70:String;
            var _loc_71:WorldPointWrapper;
            var _loc_72:String;
            var _loc_73:String;
            var _loc_74:String;
            var _loc_75:TaxCollectorAttackedMessage;
            var _loc_76:String;
            var _loc_77:String;
            var _loc_78:TaxCollectorAttackedResultMessage;
            var _loc_79:String;
            var _loc_80:String;
            var _loc_81:WorldPointWrapper;
            var _loc_82:int;
            var _loc_83:int;
            var _loc_84:TaxCollectorErrorMessage;
            var _loc_85:String;
            var _loc_86:TaxCollectorListMessage;
            var _loc_87:TaxCollectorMovementAddMessage;
            var _loc_88:Boolean;
            var _loc_89:TaxCollectorMovementRemoveMessage;
            var _loc_90:GuildInformationsPaddocksMessage;
            var _loc_91:GuildPaddockBoughtMessage;
            var _loc_92:GuildPaddockRemovedMessage;
            var _loc_93:TaxCollectorDialogQuestionExtendedMessage;
            var _loc_94:TaxCollectorDialogQuestionBasicMessage;
            var _loc_95:GuildWrapper;
            var _loc_96:ContactLookMessage;
            var _loc_97:GuildGetInformationsAction;
            var _loc_98:Boolean;
            var _loc_99:GuildInvitationAction;
            var _loc_100:GuildInvitationMessage;
            var _loc_101:GuildInvitationByNameAction;
            var _loc_102:GuildInvitationByNameMessage;
            var _loc_103:GuildKickRequestAction;
            var _loc_104:GuildKickRequestMessage;
            var _loc_105:GuildChangeMemberParametersAction;
            var _loc_106:Number;
            var _loc_107:GuildChangeMemberParametersMessage;
            var _loc_108:GuildSpellUpgradeRequestAction;
            var _loc_109:GuildSpellUpgradeRequestMessage;
            var _loc_110:GuildCharacsUpgradeRequestAction;
            var _loc_111:GuildCharacsUpgradeRequestMessage;
            var _loc_112:GuildFarmTeleportRequestAction;
            var _loc_113:GuildPaddockTeleportRequestMessage;
            var _loc_114:GuildHouseTeleportRequestAction;
            var _loc_115:GuildHouseTeleportRequestMessage;
            var _loc_116:GuildFightJoinRequestAction;
            var _loc_117:GuildFightJoinRequestMessage;
            var _loc_118:GuildFightTakePlaceRequestAction;
            var _loc_119:GuildFightTakePlaceRequestMessage;
            var _loc_120:GuildFightLeaveRequestAction;
            var _loc_121:GuildFightLeaveRequestMessage;
            var _loc_122:TaxCollectorHireRequestAction;
            var _loc_123:CharacterReportAction;
            var _loc_124:CharacterReportMessage;
            var _loc_125:ChatReportAction;
            var _loc_126:ChatMessageReportMessage;
            var _loc_127:ChatFrame;
            var _loc_128:uint;
            var _loc_129:FriendInformations;
            var _loc_130:FriendWrapper;
            var _loc_131:FriendOnlineInformations;
            var _loc_132:FriendInformations;
            var _loc_133:FriendWrapper;
            var _loc_134:*;
            var _loc_135:EnemyWrapper;
            var _loc_136:IgnoredOnlineInformations;
            var _loc_137:FriendAddRequestMessage;
            var _loc_138:IgnoredAddRequestMessage;
            var _loc_139:EnemyWrapper;
            var _loc_140:*;
            var _loc_141:*;
            var _loc_142:*;
            var _loc_143:*;
            var _loc_144:*;
            var _loc_145:*;
            var _loc_146:IgnoredAddRequestMessage;
            var _loc_147:GuildMember;
            var _loc_148:HouseInformationsForGuild;
            var _loc_149:GuildHouseWrapper;
            var _loc_150:int;
            var _loc_151:int;
            var _loc_152:GuildMember;
            var _loc_153:String;
            var _loc_154:CharacterMinimalPlusLookInformations;
            var _loc_155:GuildGetInformationsMessage;
            var _loc_156:TaxCollectorWrapper;
            var _loc_157:TaxCollectorInFightWrapper;
            var _loc_158:TaxCollectorFightersWrapper;
            var _loc_159:TaxCollectorHireRequestMessage;
            var _loc_160:GuildHouseUpdateInformationMessage;
            var _loc_161:Boolean;
            var _loc_162:GuildHouseWrapper;
            var _loc_163:GuildHouseWrapper;
            var _loc_164:GuildHouseRemoveMessage;
            var _loc_165:Boolean;
            var _loc_166:int;
            switch(true)
            {
                case param1 is GuildMembershipMessage:
                {
                    _loc_2 = param1 as GuildMembershipMessage;
                    if (this._guild != null)
                    {
                        this._guild.update(_loc_2.guildInfo.guildId, _loc_2.guildInfo.guildName, _loc_2.guildInfo.guildEmblem, _loc_2.memberRights, _loc_2.enabled);
                    }
                    else
                    {
                        this._guild = GuildWrapper.create(_loc_2.guildInfo.guildId, _loc_2.guildInfo.guildName, _loc_2.guildInfo.guildEmblem, _loc_2.memberRights, _loc_2.enabled);
                    }
                    this._hasGuild = true;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildMembership);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildMembershipUpdated, true);
                    return true;
                }
                case param1:
                {
                    _loc_3 = param1 as FriendsListMessage;
                    this._friendsList = new Array();
                    for each (_loc_129 in _loc_3.friendsList)
                    {
                        
                        if (_loc_129 is FriendOnlineInformations)
                        {
                            _loc_131 = _loc_129 as FriendOnlineInformations;
                            AccountManager.getInstance().setAccount(_loc_131.playerName, _loc_131.accountId, _loc_131.accountName);
                            ChatAutocompleteNameManager.getInstance().addEntry((_loc_129 as FriendOnlineInformations).playerName, 2);
                        }
                        _loc_130 = new FriendWrapper(_loc_129);
                        this._friendsList.push(_loc_130);
                    }
                    if (this._spouse)
                    {
                        this._spouse = null;
                        KernelEventsManager.getInstance().processCallback(SocialHookList.SpouseUpdated);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendsListUpdated);
                    return true;
                }
                case getQualifiedClassName(param1) == getQualifiedClassName(FriendsListMessage):
                {
                    this._friendsList = new Array();
                    _loc_4 = param1 as FriendsListWithSpouseMessage;
                    _loc_5 = new SpouseWrapper(_loc_4.spouse);
                    this._spouse = _loc_5;
                    for each (_loc_132 in _loc_4.friendsList)
                    {
                        
                        if (_loc_132 is FriendOnlineInformations)
                        {
                            ChatAutocompleteNameManager.getInstance().addEntry((_loc_132 as FriendOnlineInformations).playerName, 2);
                        }
                        _loc_133 = new FriendWrapper(_loc_132);
                        this._friendsList.push(_loc_133);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendsListUpdated);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.SpouseUpdated);
                    return true;
                }
                case param1 is FriendsListWithSpouseMessage:
                {
                    this._enemiesList = new Array();
                    _loc_6 = param1 as IgnoredListMessage;
                    for each (_loc_134 in _loc_6.ignoredList)
                    {
                        
                        if (_loc_134 is IgnoredOnlineInformations)
                        {
                            _loc_136 = _loc_129 as IgnoredOnlineInformations;
                            AccountManager.getInstance().setAccount(_loc_136.playerName, _loc_136.accountId, _loc_136.accountName);
                        }
                        _loc_135 = new EnemyWrapper(_loc_134);
                        this._enemiesList.push(_loc_135);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.EnemiesListUpdated);
                    return true;
                }
                case param1 is IgnoredListMessage:
                {
                    _loc_7 = param1 as OpenSocialAction;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.OpenSocial, _loc_7.name);
                    return true;
                }
                case param1 is OpenSocialAction:
                {
                    _loc_8 = param1 as FriendsListRequestAction;
                    ConnectionsHandler.getConnection().send(new FriendsGetListMessage());
                    return true;
                }
                case param1 is FriendsListRequestAction:
                {
                    _loc_9 = param1 as EnemiesListRequestAction;
                    ConnectionsHandler.getConnection().send(new IgnoredGetListMessage());
                    return true;
                }
                case param1 is EnemiesListRequestAction:
                {
                    _loc_10 = param1 as SpouseRequestAction;
                    ConnectionsHandler.getConnection().send(new FriendsGetListMessage());
                    return true;
                }
                case param1 is SpouseRequestAction:
                {
                    _loc_11 = param1 as AddFriendAction;
                    if (_loc_11.name.length < 2 || _loc_11.name.length > 20)
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureNotFound");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    else if (_loc_11.name != PlayedCharacterManager.getInstance().infos.name)
                    {
                        _loc_137 = new FriendAddRequestMessage();
                        _loc_137.initFriendAddRequestMessage(_loc_11.name);
                        ConnectionsHandler.getConnection().send(_loc_137);
                    }
                    else
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureEgocentric");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    return true;
                }
                case param1 is AddFriendAction:
                {
                    _loc_12 = param1 as FriendAddedMessage;
                    if (_loc_12.friendAdded is FriendOnlineInformations)
                    {
                        _loc_131 = _loc_12.friendAdded as FriendOnlineInformations;
                        AccountManager.getInstance().setAccount(_loc_131.playerName, _loc_131.accountId, _loc_131.accountName);
                        ChatAutocompleteNameManager.getInstance().addEntry((_loc_12.friendAdded as FriendInformations).accountName, 2);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendAdded, true);
                    _loc_13 = new FriendWrapper(_loc_12.friendAdded);
                    this._friendsList.push(_loc_13);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendsListUpdated);
                    return true;
                }
                case param1 is FriendAddedMessage:
                {
                    _loc_14 = param1 as FriendAddFailureMessage;
                    switch(_loc_14.reason)
                    {
                        case ListAddFailureEnum.LIST_ADD_FAILURE_UNKNOWN:
                        {
                            _loc_15 = I18n.getUiText("ui.common.unknowReason");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_OVER_QUOTA:
                        {
                            _loc_15 = I18n.getUiText("ui.social.friend.addFailureListFull");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_NOT_FOUND:
                        {
                            _loc_15 = I18n.getUiText("ui.social.friend.addFailureNotFound");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_EGOCENTRIC:
                        {
                            _loc_15 = I18n.getUiText("ui.social.friend.addFailureEgocentric");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_IS_DOUBLE:
                        {
                            _loc_15 = I18n.getUiText("ui.social.friend.addFailureAlreadyInList");
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    return true;
                }
                case param1 is FriendAddFailureMessage:
                {
                    _loc_16 = param1 as AddEnemyAction;
                    if (_loc_16.name.length < 2 || _loc_16.name.length > 20)
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureNotFound");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    else if (_loc_16.name != PlayedCharacterManager.getInstance().infos.name)
                    {
                        _loc_138 = new IgnoredAddRequestMessage();
                        _loc_138.initIgnoredAddRequestMessage(_loc_16.name);
                        ConnectionsHandler.getConnection().send(_loc_138);
                    }
                    else
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureEgocentric");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    return true;
                }
                case param1 is AddEnemyAction:
                {
                    _loc_17 = param1 as IgnoredAddedMessage;
                    if (_loc_17.ignoreAdded is IgnoredOnlineInformations)
                    {
                        _loc_136 = _loc_17.ignoreAdded as IgnoredOnlineInformations;
                        AccountManager.getInstance().setAccount(_loc_136.playerName, _loc_136.accountId, _loc_136.accountName);
                    }
                    if (!_loc_17.session)
                    {
                        KernelEventsManager.getInstance().processCallback(SocialHookList.EnemyAdded, true);
                        _loc_139 = new EnemyWrapper(_loc_17.ignoreAdded);
                        this._enemiesList.push(_loc_139);
                        KernelEventsManager.getInstance().processCallback(SocialHookList.EnemiesListUpdated);
                    }
                    else
                    {
                        for each (_loc_140 in this._ignoredList)
                        {
                            
                            if (_loc_140.name == _loc_17.ignoreAdded.accountName)
                            {
                                return true;
                            }
                        }
                        this._ignoredList.push(new IgnoredWrapper(_loc_17.ignoreAdded.accountName, _loc_17.ignoreAdded.accountId));
                        KernelEventsManager.getInstance().processCallback(SocialHookList.IgnoredAdded);
                    }
                    return true;
                }
                case param1 is IgnoredAddedMessage:
                {
                    _loc_18 = param1 as IgnoredAddFailureMessage;
                    switch(_loc_18.reason)
                    {
                        case ListAddFailureEnum.LIST_ADD_FAILURE_UNKNOWN:
                        {
                            _loc_19 = I18n.getUiText("ui.common.unknowReason");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_OVER_QUOTA:
                        {
                            _loc_19 = I18n.getUiText("ui.social.friend.addFailureListFull");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_NOT_FOUND:
                        {
                            _loc_19 = I18n.getUiText("ui.social.friend.addFailureNotFound");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_EGOCENTRIC:
                        {
                            _loc_19 = I18n.getUiText("ui.social.friend.addFailureEgocentric");
                            break;
                        }
                        case ListAddFailureEnum.LIST_ADD_FAILURE_IS_DOUBLE:
                        {
                            _loc_19 = I18n.getUiText("ui.social.friend.addFailureAlreadyInList");
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_19, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    return true;
                }
                case param1 is IgnoredAddFailureMessage:
                {
                    _loc_20 = param1 as RemoveFriendAction;
                    _loc_21 = new FriendDeleteRequestMessage();
                    _loc_21.initFriendDeleteRequestMessage(_loc_20.name);
                    ConnectionsHandler.getConnection().send(_loc_21);
                    return true;
                }
                case param1 is RemoveFriendAction:
                {
                    _loc_22 = param1 as FriendDeleteResultMessage;
                    _loc_23 = I18n.getUiText("ui.social.friend.delete");
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendRemoved, _loc_22.success);
                    if (_loc_22.success)
                    {
                        for (_loc_141 in this._friendsList)
                        {
                            
                            if (this._friendsList[_loc_141].name == _loc_22.name)
                            {
                                this._friendsList.splice(_loc_141, 1);
                            }
                        }
                        KernelEventsManager.getInstance().processCallback(SocialHookList.FriendsListUpdated);
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_23, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    return true;
                }
                case param1 is FriendDeleteResultMessage:
                {
                    _loc_24 = param1 as FriendUpdateMessage;
                    _loc_25 = new FriendWrapper(_loc_24.friendUpdated);
                    for each (_loc_142 in this._friendsList)
                    {
                        
                        if (_loc_142.name == _loc_25.name)
                        {
                            _loc_142 = _loc_25;
                            break;
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendsListUpdated);
                    if (AirScanner.hasAir() && _loc_25.online && this._warnOnFrienConnec)
                    {
                        KernelEventsManager.getInstance().processCallback(HookList.ExternalNotification, ExternalNotificationTypeEnum.FRIEND_CONNECTION, [_loc_25.name, _loc_25.playerName]);
                    }
                    return true;
                }
                case param1 is FriendUpdateMessage:
                {
                    _loc_26 = param1 as RemoveEnemyAction;
                    _loc_27 = new IgnoredDeleteRequestMessage();
                    _loc_27.initIgnoredDeleteRequestMessage(_loc_26.name);
                    ConnectionsHandler.getConnection().send(_loc_27);
                    return true;
                }
                case param1 is RemoveEnemyAction:
                {
                    _loc_28 = param1 as IgnoredDeleteResultMessage;
                    if (!_loc_28.session)
                    {
                        KernelEventsManager.getInstance().processCallback(SocialHookList.EnemyRemoved, _loc_28.success);
                        if (_loc_28.success)
                        {
                            for (_loc_143 in this._enemiesList)
                            {
                                
                                if (this._enemiesList[_loc_143].name == _loc_28.name)
                                {
                                    this._enemiesList.splice(_loc_143, 1);
                                }
                            }
                        }
                        KernelEventsManager.getInstance().processCallback(SocialHookList.EnemiesListUpdated);
                    }
                    else if (_loc_28.success)
                    {
                        for (_loc_144 in this._ignoredList)
                        {
                            
                            if (this._ignoredList[_loc_144].name == _loc_28.name)
                            {
                                this._ignoredList.splice(_loc_144, 1);
                            }
                        }
                        KernelEventsManager.getInstance().processCallback(SocialHookList.IgnoredRemoved);
                    }
                    return true;
                }
                case param1 is IgnoredDeleteResultMessage:
                {
                    _loc_29 = param1 as AddIgnoredAction;
                    if (_loc_29.name.length < 2 || _loc_29.name.length > 20)
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureNotFound");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    else if (_loc_29.name != PlayedCharacterManager.getInstance().infos.name)
                    {
                        for each (_loc_145 in this._ignoredList)
                        {
                            
                            _log.debug(" " + _loc_145.playerName + " == " + _loc_29.name);
                            if (_loc_145.playerName == _loc_29.name)
                            {
                                return true;
                            }
                        }
                        _loc_146 = new IgnoredAddRequestMessage();
                        _loc_146.initIgnoredAddRequestMessage(_loc_29.name, true);
                        ConnectionsHandler.getConnection().send(_loc_146);
                    }
                    else
                    {
                        _loc_15 = I18n.getUiText("ui.social.friend.addFailureEgocentric");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_15, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    return true;
                }
                case param1 is AddIgnoredAction:
                {
                    _loc_30 = param1 as RemoveIgnoredAction;
                    _loc_31 = new IgnoredDeleteRequestMessage();
                    _loc_31.initIgnoredDeleteRequestMessage(_loc_30.name, true);
                    ConnectionsHandler.getConnection().send(_loc_31);
                    return true;
                }
                case param1 is RemoveIgnoredAction:
                {
                    _loc_32 = param1 as JoinFriendAction;
                    _loc_33 = new FriendJoinRequestMessage();
                    _loc_33.initFriendJoinRequestMessage(_loc_32.name);
                    ConnectionsHandler.getConnection().send(_loc_33);
                    return true;
                }
                case param1 is JoinFriendAction:
                {
                    _loc_34 = param1 as JoinSpouseAction;
                    ConnectionsHandler.getConnection().send(new FriendSpouseJoinRequestMessage());
                    return true;
                }
                case param1 is JoinSpouseAction:
                {
                    _loc_35 = param1 as FriendSpouseFollowAction;
                    _loc_36 = new FriendSpouseFollowWithCompassRequestMessage();
                    _loc_36.initFriendSpouseFollowWithCompassRequestMessage(_loc_35.enable);
                    ConnectionsHandler.getConnection().send(_loc_36);
                    return true;
                }
                case param1 is FriendSpouseFollowAction:
                {
                    _loc_37 = param1 as FriendWarningSetAction;
                    this._warnOnFrienConnec = _loc_37.enable;
                    _loc_38 = new FriendSetWarnOnConnectionMessage();
                    _loc_38.initFriendSetWarnOnConnectionMessage(_loc_37.enable);
                    ConnectionsHandler.getConnection().send(_loc_38);
                    return true;
                }
                case param1 is FriendWarningSetAction:
                {
                    _loc_39 = param1 as MemberWarningSetAction;
                    this._warnOnMemberConnec = _loc_39.enable;
                    _loc_40 = new GuildMemberSetWarnOnConnectionMessage();
                    _loc_40.initGuildMemberSetWarnOnConnectionMessage(_loc_39.enable);
                    ConnectionsHandler.getConnection().send(_loc_40);
                    return true;
                }
                case param1 is MemberWarningSetAction:
                {
                    _loc_41 = param1 as FriendOrGuildMemberLevelUpWarningSetAction;
                    this._warnWhenFriendOrGuildMemberLvlUp = _loc_41.enable;
                    _loc_42 = new FriendSetWarnOnLevelGainMessage();
                    _loc_42.initFriendSetWarnOnLevelGainMessage(_loc_41.enable);
                    ConnectionsHandler.getConnection().send(_loc_42);
                    return true;
                }
                case param1 is FriendOrGuildMemberLevelUpWarningSetAction:
                {
                    _loc_43 = param1 as SpouseStatusMessage;
                    this._hasSpouse = _loc_43.hasSpouse;
                    if (!this._hasSpouse)
                    {
                        this._spouse = null;
                        KernelEventsManager.getInstance().processCallback(SocialHookList.SpouseFollowStatusUpdated, false);
                        KernelEventsManager.getInstance().processCallback(HookList.RemoveMapFlag, "flag_srv" + CompassTypeEnum.COMPASS_TYPE_SPOUSE);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.SpouseUpdated);
                    return true;
                }
                case param1 is SpouseStatusMessage:
                {
                    _loc_44 = param1 as FriendWarnOnConnectionStateMessage;
                    this._warnOnFrienConnec = _loc_44.enable;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendWarningState, _loc_44.enable);
                    return true;
                }
                case param1 is FriendWarnOnConnectionStateMessage:
                {
                    _loc_45 = param1 as GuildMemberWarnOnConnectionStateMessage;
                    this._warnOnMemberConnec = _loc_45.enable;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.MemberWarningState, _loc_45.enable);
                    return true;
                }
                case param1 is GuildMemberWarnOnConnectionStateMessage:
                {
                    _loc_46 = param1 as FriendWarnOnLevelGainStateMessage;
                    this._warnWhenFriendOrGuildMemberLvlUp = _loc_46.enable;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.FriendOrGuildMemberLevelUpWarningState, _loc_46.enable);
                    return true;
                }
                case param1 is FriendWarnOnLevelGainStateMessage:
                {
                    _loc_47 = param1 as GuildInformationsMembersMessage;
                    for each (_loc_147 in _loc_47.members)
                    {
                        
                        ChatAutocompleteNameManager.getInstance().addEntry(_loc_147.name, 2);
                    }
                    this._guildMembers = _loc_47.members;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsMembers, this._guildMembers);
                    return true;
                }
                case param1 is GuildInformationsMembersMessage:
                {
                    _loc_48 = param1 as GuildHousesInformationMessage;
                    this._guildHouses = new Vector.<GuildHouseWrapper>;
                    for each (_loc_148 in _loc_48.housesInformations)
                    {
                        
                        _loc_149 = GuildHouseWrapper.create(_loc_148);
                        this._guildHouses.push(_loc_149);
                    }
                    this._guildHousesList = true;
                    this._guildHousesListUpdate = true;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildHousesUpdate);
                    return true;
                }
                case param1 is GuildHousesInformationMessage:
                {
                    Kernel.getWorker().addFrame(this._guildDialogFrame);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildCreationStarted, false, false);
                    return true;
                }
                case param1 is GuildCreationStartedMessage:
                {
                    _loc_49 = param1 as GuildModificationStartedMessage;
                    Kernel.getWorker().addFrame(this._guildDialogFrame);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildCreationStarted, _loc_49.canChangeName, _loc_49.canChangeEmblem);
                    return true;
                }
                case param1 is GuildModificationStartedMessage:
                {
                    _loc_50 = param1 as GuildCreationResultMessage;
                    switch(_loc_50.result)
                    {
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_ALREADY_IN_GUILD:
                        {
                            _loc_51 = I18n.getUiText("ui.guild.alreadyInGuild");
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_CANCEL:
                        {
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_EMBLEM_ALREADY_EXISTS:
                        {
                            _loc_51 = I18n.getUiText("ui.guild.AlreadyUseEmblem");
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_LEAVE:
                        {
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_NAME_ALREADY_EXISTS:
                        {
                            _loc_51 = I18n.getUiText("ui.guild.AlreadyUseName");
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_NAME_INVALID:
                        {
                            _loc_51 = I18n.getUiText("ui.guild.invalidName");
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_ERROR_REQUIREMENT_UNMET:
                        {
                            _loc_51 = I18n.getUiText("ui.guild.requirementUnmet");
                            break;
                        }
                        case GuildCreationResultEnum.GUILD_CREATE_OK:
                        {
                            Kernel.getWorker().removeFrame(this._guildDialogFrame);
                            this._hasGuild = true;
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    if (_loc_51)
                    {
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_51, ChatActivableChannelsEnum.PSEUDO_CHANNEL_INFO, TimeManager.getInstance().getTimestamp());
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildCreationResult, _loc_50.result);
                    return true;
                }
                case param1 is GuildCreationResultMessage:
                {
                    _loc_52 = param1 as GuildInvitedMessage;
                    Kernel.getWorker().addFrame(this._guildDialogFrame);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInvited, _loc_52.guildInfo.guildName, _loc_52.recruterId, _loc_52.recruterName);
                    return true;
                }
                case param1 is GuildInvitedMessage:
                {
                    _loc_53 = param1 as GuildInvitationStateRecruterMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInvitationStateRecruter, _loc_53.invitationState, _loc_53.recrutedName);
                    if (_loc_53.invitationState == 2 || _loc_53.invitationState == 3)
                    {
                        Kernel.getWorker().removeFrame(this._guildDialogFrame);
                    }
                    return true;
                }
                case param1 is GuildInvitationStateRecruterMessage:
                {
                    _loc_54 = param1 as GuildInvitationStateRecrutedMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInvitationStateRecruted, _loc_54.invitationState);
                    return true;
                }
                case param1 is GuildInvitationStateRecrutedMessage:
                {
                    _loc_55 = param1 as GuildJoinedMessage;
                    this._hasGuild = true;
                    this._guild = GuildWrapper.create(_loc_55.guildInfo.guildId, _loc_55.guildInfo.guildName, _loc_55.guildInfo.guildEmblem, _loc_55.memberRights, _loc_55.enabled);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildJoined, _loc_55.guildInfo.guildEmblem, _loc_55.guildInfo.guildName, _loc_55.memberRights);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildMembershipUpdated, true);
                    _loc_56 = I18n.getUiText("ui.guild.JoinGuildMessage", [_loc_55.guildInfo.guildName]);
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_56, ChatActivableChannelsEnum.PSEUDO_CHANNEL_INFO, TimeManager.getInstance().getTimestamp());
                    return true;
                }
                case param1 is GuildJoinedMessage:
                {
                    _loc_57 = param1 as GuildUIOpenedMessage;
                    if (this._guild != null)
                    {
                        KernelEventsManager.getInstance().processCallback(SocialHookList.GuildUIOpened, _loc_57.type);
                    }
                    return true;
                }
                case param1 is GuildUIOpenedMessage:
                {
                    _loc_58 = param1 as GuildInformationsGeneralMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsGeneral, _loc_58.enabled, _loc_58.expLevelFloor, _loc_58.experience, _loc_58.expNextLevelFloor, _loc_58.level, _loc_58.creationDate, _loc_58.abandonnedPaddock);
                    this._guild.level = _loc_58.level;
                    this._guild.experience = _loc_58.experience;
                    this._guild.expLevelFloor = _loc_58.expLevelFloor;
                    this._guild.expNextLevelFloor = _loc_58.expNextLevelFloor;
                    this._guild.creationDate = _loc_58.creationDate;
                    return true;
                }
                case param1 is GuildInformationsGeneralMessage:
                {
                    _loc_59 = param1 as GuildInformationsMemberUpdateMessage;
                    if (this._guildMembers != null)
                    {
                        _loc_150 = this._guildMembers.length;
                        _loc_151 = 0;
                        while (_loc_151 < _loc_150)
                        {
                            
                            _loc_60 = this._guildMembers[_loc_151];
                            if (_loc_60.id == _loc_59.member.id)
                            {
                                this._guildMembers[_loc_151] = _loc_59.member;
                                if (_loc_60.id == PlayedCharacterManager.getInstance().id)
                                {
                                    this.guild.memberRightsNumber = _loc_59.member.rights;
                                }
                                break;
                            }
                            _loc_151++;
                        }
                    }
                    else
                    {
                        this._guildMembers = new Vector.<GuildMember>;
                        _loc_60 = _loc_59.member;
                        this._guildMembers.push(_loc_60);
                        if (_loc_60.id == PlayedCharacterManager.getInstance().id)
                        {
                            this.guild.memberRightsNumber = _loc_60.rights;
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsMembers, this._guildMembers);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsMemberUpdate, _loc_59.member);
                    return true;
                }
                case param1 is GuildInformationsMemberUpdateMessage:
                {
                    _loc_61 = param1 as GuildMemberLeavingMessage;
                    _loc_62 = 0;
                    for each (_loc_152 in this._guildMembers)
                    {
                        
                        if (_loc_61.memberId == _loc_152.id)
                        {
                            this._guildMembers.splice(_loc_62, 1);
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsMembers, this._guildMembers);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildMemberLeaving, _loc_61.kicked, _loc_61.memberId);
                    return true;
                }
                case param1 is GuildMemberLeavingMessage:
                {
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildLeft);
                    this._hasGuild = false;
                    this._guild = null;
                    this._guildHousesList = false;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildMembershipUpdated, false);
                    return true;
                }
                case param1 is GuildLeftMessage:
                {
                    _loc_63 = param1 as GuildInfosUpgradeMessage;
                    TaxCollectorsManager.getInstance().updateGuild(_loc_63.maxTaxCollectorsCount, _loc_63.taxCollectorsCount, _loc_63.taxCollectorLifePoints, _loc_63.taxCollectorDamagesBonuses, _loc_63.taxCollectorPods, _loc_63.taxCollectorProspecting, _loc_63.taxCollectorWisdom);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInfosUpgrade, _loc_63.boostPoints, _loc_63.maxTaxCollectorsCount, _loc_63.spellId, _loc_63.spellLevel, _loc_63.taxCollectorDamagesBonuses, _loc_63.taxCollectorLifePoints, _loc_63.taxCollectorPods, _loc_63.taxCollectorProspecting, _loc_63.taxCollectorsCount, _loc_63.taxCollectorWisdom);
                    return true;
                }
                case param1 is GuildInfosUpgradeMessage:
                {
                    _loc_64 = param1 as GuildFightPlayersHelpersJoinMessage;
                    TaxCollectorsManager.getInstance().addFighter(_loc_64.fightId, _loc_64.playerInfo, true);
                    return true;
                }
                case param1 is GuildFightPlayersHelpersJoinMessage:
                {
                    _loc_65 = param1 as GuildFightPlayersHelpersLeaveMessage;
                    if (this._autoLeaveHelpers)
                    {
                        _loc_153 = I18n.getUiText("ui.social.guild.autoFightLeave");
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_153, ChatActivableChannelsEnum.PSEUDO_CHANNEL_INFO, TimeManager.getInstance().getTimestamp());
                    }
                    TaxCollectorsManager.getInstance().removeFighter(_loc_65.fightId, _loc_65.playerId, true);
                    return true;
                }
                case param1 is GuildFightPlayersHelpersLeaveMessage:
                {
                    _loc_66 = param1 as GuildFightPlayersEnemiesListMessage;
                    for each (_loc_154 in _loc_66.playerInfo)
                    {
                        
                        TaxCollectorsManager.getInstance().addFighter(_loc_66.fightId, _loc_154, false, false);
                    }
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildFightEnnemiesListUpdate, _loc_66.fightId);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.TaxCollectorUpdate, _loc_66.fightId);
                    return true;
                }
                case param1 is GuildFightPlayersEnemiesListMessage:
                {
                    _loc_67 = param1 as GuildFightPlayersEnemyRemoveMessage;
                    TaxCollectorsManager.getInstance().removeFighter(_loc_67.fightId, _loc_67.playerId, false);
                    return true;
                }
                case param1 is GuildFightPlayersEnemyRemoveMessage:
                {
                    _loc_68 = param1 as TaxCollectorMovementMessage;
                    _loc_70 = TaxCollectorFirstname.getTaxCollectorFirstnameById(_loc_68.basicInfos.firstNameId).firstname + " " + TaxCollectorName.getTaxCollectorNameById(_loc_68.basicInfos.lastNameId).name;
                    _loc_71 = new WorldPointWrapper(_loc_68.basicInfos.mapId, true, _loc_68.basicInfos.worldX, _loc_68.basicInfos.worldY);
                    _loc_72 = String(_loc_71.outdoorX);
                    _loc_73 = String(_loc_71.outdoorY);
                    _loc_74 = _loc_72 + "," + _loc_73;
                    switch(_loc_68.hireOrFire)
                    {
                        case true:
                        {
                            _loc_69 = I18n.getUiText("ui.social.TaxCollectorAdded", [_loc_70, _loc_74, _loc_68.playerName]);
                            KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_69, ChatActivableChannelsEnum.CHANNEL_GUILD, TimeManager.getInstance().getTimestamp());
                            break;
                        }
                        case false:
                        {
                            _loc_69 = I18n.getUiText("ui.social.TaxCollectorRemoved", [_loc_70, _loc_74, _loc_68.playerName]);
                            KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_69, ChatActivableChannelsEnum.CHANNEL_GUILD, TimeManager.getInstance().getTimestamp());
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    return true;
                }
                case param1 is TaxCollectorMovementMessage:
                {
                    _loc_75 = param1 as TaxCollectorAttackedMessage;
                    _loc_76 = TaxCollectorFirstname.getTaxCollectorFirstnameById(_loc_75.firstNameId).firstname + " " + TaxCollectorName.getTaxCollectorNameById(_loc_75.lastNameId).name;
                    _loc_77 = I18n.getUiText("ui.social.TaxCollectorAttacked", [_loc_76, _loc_75.worldX + "," + _loc_75.worldY]);
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, "{openSocial,1,2::" + _loc_77 + "}", ChatActivableChannelsEnum.CHANNEL_GUILD, TimeManager.getInstance().getTimestamp());
                    if (AirScanner.hasAir())
                    {
                        KernelEventsManager.getInstance().processCallback(HookList.ExternalNotification, ExternalNotificationTypeEnum.TAXCOLLECTOR_ATTACK, [_loc_76, _loc_75.worldX, _loc_75.worldY]);
                    }
                    return true;
                }
                case param1 is TaxCollectorAttackedMessage:
                {
                    _loc_78 = param1 as TaxCollectorAttackedResultMessage;
                    _loc_80 = TaxCollectorFirstname.getTaxCollectorFirstnameById(_loc_78.basicInfos.firstNameId).firstname + " " + TaxCollectorName.getTaxCollectorNameById(_loc_78.basicInfos.lastNameId).name;
                    _loc_81 = new WorldPointWrapper(_loc_78.basicInfos.mapId, true, _loc_78.basicInfos.worldX, _loc_78.basicInfos.worldY);
                    _loc_82 = _loc_81.outdoorX;
                    _loc_83 = _loc_81.outdoorY;
                    if (_loc_78.deadOrAlive)
                    {
                        _loc_79 = I18n.getUiText("ui.social.TaxCollectorDied", [_loc_80, _loc_82 + "," + _loc_83]);
                    }
                    else
                    {
                        _loc_79 = I18n.getUiText("ui.social.TaxCollectorSurvived", [_loc_80, _loc_82 + "," + _loc_83]);
                    }
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_79, ChatActivableChannelsEnum.CHANNEL_GUILD, TimeManager.getInstance().getTimestamp());
                    return true;
                }
                case param1 is TaxCollectorAttackedResultMessage:
                {
                    _loc_84 = param1 as TaxCollectorErrorMessage;
                    _loc_85 = "";
                    switch(_loc_84.reason)
                    {
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_ALREADY_ONE:
                        {
                            _loc_85 = I18n.getUiText("ui.social.alreadyTaxCollectorOnMap");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_CANT_HIRE_HERE:
                        {
                            _loc_85 = I18n.getUiText("ui.social.cantHireTaxCollecotrHere");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_CANT_HIRE_YET:
                        {
                            _loc_85 = I18n.getUiText("ui.social.cantHireTaxcollectorTooTired");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_ERROR_UNKNOWN:
                        {
                            _loc_85 = I18n.getUiText("ui.social.unknownErrorTaxCollector");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_MAX_REACHED:
                        {
                            _loc_85 = I18n.getUiText("ui.social.cantHireMaxTaxCollector");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_NO_RIGHTS:
                        {
                            _loc_85 = I18n.getUiText("ui.social.taxCollectorNoRights");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_NOT_ENOUGH_KAMAS:
                        {
                            _loc_85 = I18n.getUiText("ui.social.notEnougthRichToHireTaxCollector");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_NOT_FOUND:
                        {
                            _loc_85 = I18n.getUiText("ui.social.taxCollectorNotFound");
                            break;
                        }
                        case TaxCollectorErrorReasonEnum.TAX_COLLECTOR_NOT_OWNED:
                        {
                            _loc_85 = I18n.getUiText("ui.social.notYourTaxcollector");
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, _loc_85, ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    return true;
                }
                case param1 is TaxCollectorErrorMessage:
                {
                    _loc_86 = param1 as TaxCollectorListMessage;
                    TaxCollectorsManager.getInstance().taxCollectorHireCost = _loc_86.taxCollectorHireCost;
                    TaxCollectorsManager.getInstance().maxTaxCollectorsCount = _loc_86.nbcollectorMax;
                    TaxCollectorsManager.getInstance().setTaxCollectors(_loc_86.informations);
                    TaxCollectorsManager.getInstance().setTaxCollectorsFighters(_loc_86.fightersInformations);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.TaxCollectorListUpdate);
                    return true;
                }
                case param1 is TaxCollectorListMessage:
                {
                    _loc_87 = param1 as TaxCollectorMovementAddMessage;
                    _loc_88 = TaxCollectorsManager.getInstance().addTaxCollector(_loc_87.informations);
                    if (_loc_88 || TaxCollectorsManager.getInstance().taxCollectors[_loc_87.informations.uniqueId].state != 1)
                    {
                        KernelEventsManager.getInstance().processCallback(SocialHookList.TaxCollectorUpdate, _loc_87.informations.uniqueId);
                    }
                    if (_loc_88)
                    {
                        KernelEventsManager.getInstance().processCallback(SocialHookList.GuildTaxCollectorAdd, _loc_87.informations);
                    }
                    return true;
                }
                case param1 is TaxCollectorMovementAddMessage:
                {
                    _loc_89 = param1 as TaxCollectorMovementRemoveMessage;
                    delete TaxCollectorsManager.getInstance().taxCollectors[_loc_89.collectorId];
                    delete TaxCollectorsManager.getInstance().taxCollectorsFighters[_loc_89.collectorId];
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildTaxCollectorRemoved, _loc_89.collectorId);
                    return true;
                }
                case param1 is TaxCollectorMovementRemoveMessage:
                {
                    _loc_90 = param1 as GuildInformationsPaddocksMessage;
                    this._guildPaddocksMax = _loc_90.nbPaddockMax;
                    this._guildPaddocks = _loc_90.paddocksInformations;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildInformationsFarms);
                    return true;
                }
                case param1 is GuildInformationsPaddocksMessage:
                {
                    _loc_91 = param1 as GuildPaddockBoughtMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildPaddockAdd, _loc_91.paddockInfo);
                    return true;
                }
                case param1 is GuildPaddockBoughtMessage:
                {
                    _loc_92 = param1 as GuildPaddockRemovedMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildPaddockRemoved, _loc_92.paddockId);
                    return true;
                }
                case param1 is GuildPaddockRemovedMessage:
                {
                    _loc_93 = param1 as TaxCollectorDialogQuestionExtendedMessage;
                    KernelEventsManager.getInstance().processCallback(SocialHookList.TaxCollectorDialogQuestionExtended, _loc_93.guildInfo.guildName, _loc_93.maxPods, _loc_93.prospecting, _loc_93.wisdom, _loc_93.taxCollectorsCount, _loc_93.taxCollectorAttack, _loc_93.kamas, _loc_93.experience, _loc_93.pods, _loc_93.itemsValue);
                    return true;
                }
                case param1 is TaxCollectorDialogQuestionExtendedMessage:
                {
                    _loc_94 = param1 as TaxCollectorDialogQuestionBasicMessage;
                    _loc_95 = GuildWrapper.create(0, _loc_94.guildInfo.guildName, null, 0, true);
                    KernelEventsManager.getInstance().processCallback(SocialHookList.TaxCollectorDialogQuestionBasic, _loc_95.guildName);
                    return true;
                }
                case param1 is TaxCollectorDialogQuestionBasicMessage:
                {
                    _loc_96 = param1 as ContactLookMessage;
                    KernelEventsManager.getInstance().processCallback(CraftHookList.JobCrafterContactLook, _loc_96.playerId, _loc_96.playerName, EntityLookAdapter.fromNetwork(_loc_96.look));
                    return true;
                }
                case param1 is ContactLookMessage:
                {
                    _loc_97 = param1 as GuildGetInformationsAction;
                    _loc_98 = true;
                    switch(_loc_97.infoType)
                    {
                        case GuildInformationsTypeEnum.INFO_MEMBERS:
                        {
                            break;
                        }
                        case GuildInformationsTypeEnum.INFO_TAX_COLLECTOR:
                        {
                            break;
                        }
                        case GuildInformationsTypeEnum.INFO_HOUSES:
                        {
                            if (this._guildHousesList)
                            {
                                _loc_98 = false;
                                if (this._guildHousesListUpdate)
                                {
                                    this._guildHousesListUpdate = false;
                                    KernelEventsManager.getInstance().processCallback(SocialHookList.GuildHousesUpdate);
                                }
                            }
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    if (_loc_98)
                    {
                        _loc_155 = new GuildGetInformationsMessage();
                        _loc_155.initGuildGetInformationsMessage(_loc_97.infoType);
                        ConnectionsHandler.getConnection().send(_loc_155);
                    }
                    return true;
                }
                case param1 is GuildGetInformationsAction:
                {
                    _loc_99 = param1 as GuildInvitationAction;
                    Kernel.getWorker().addFrame(this._guildDialogFrame);
                    _loc_100 = new GuildInvitationMessage();
                    _loc_100.initGuildInvitationMessage(_loc_99.targetId);
                    ConnectionsHandler.getConnection().send(_loc_100);
                    return true;
                }
                case param1 is GuildInvitationAction:
                {
                    _loc_101 = param1 as GuildInvitationByNameAction;
                    Kernel.getWorker().addFrame(this._guildDialogFrame);
                    _loc_102 = new GuildInvitationByNameMessage();
                    _loc_102.initGuildInvitationByNameMessage(_loc_101.target);
                    ConnectionsHandler.getConnection().send(_loc_102);
                    return true;
                }
                case param1 is GuildInvitationByNameAction:
                {
                    _loc_103 = param1 as GuildKickRequestAction;
                    _loc_104 = new GuildKickRequestMessage();
                    _loc_104.initGuildKickRequestMessage(_loc_103.targetId);
                    ConnectionsHandler.getConnection().send(_loc_104);
                    return true;
                }
                case param1 is GuildKickRequestAction:
                {
                    _loc_105 = param1 as GuildChangeMemberParametersAction;
                    _loc_106 = GuildWrapper.getRightsNumber(_loc_105.rights);
                    _loc_107 = new GuildChangeMemberParametersMessage();
                    _loc_107.initGuildChangeMemberParametersMessage(_loc_105.memberId, _loc_105.rank, _loc_105.experienceGivenPercent, _loc_106);
                    ConnectionsHandler.getConnection().send(_loc_107);
                    return true;
                }
                case param1 is GuildChangeMemberParametersAction:
                {
                    _loc_108 = param1 as GuildSpellUpgradeRequestAction;
                    _loc_109 = new GuildSpellUpgradeRequestMessage();
                    _loc_109.initGuildSpellUpgradeRequestMessage(_loc_108.spellId);
                    ConnectionsHandler.getConnection().send(_loc_109);
                    return true;
                }
                case param1 is GuildSpellUpgradeRequestAction:
                {
                    _loc_110 = param1 as GuildCharacsUpgradeRequestAction;
                    _loc_111 = new GuildCharacsUpgradeRequestMessage();
                    _loc_111.initGuildCharacsUpgradeRequestMessage(_loc_110.charaTypeTarget);
                    ConnectionsHandler.getConnection().send(_loc_111);
                    return true;
                }
                case param1 is GuildCharacsUpgradeRequestAction:
                {
                    _loc_112 = param1 as GuildFarmTeleportRequestAction;
                    _loc_113 = new GuildPaddockTeleportRequestMessage();
                    _loc_113.initGuildPaddockTeleportRequestMessage(_loc_112.farmId);
                    ConnectionsHandler.getConnection().send(_loc_113);
                    return true;
                }
                case param1 is GuildFarmTeleportRequestAction:
                {
                    _loc_114 = param1 as GuildHouseTeleportRequestAction;
                    _loc_115 = new GuildHouseTeleportRequestMessage();
                    _loc_115.initGuildHouseTeleportRequestMessage(_loc_114.houseId);
                    ConnectionsHandler.getConnection().send(_loc_115);
                    return true;
                }
                case param1 is GuildHouseTeleportRequestAction:
                {
                    _loc_116 = param1 as GuildFightJoinRequestAction;
                    _loc_117 = new GuildFightJoinRequestMessage();
                    _loc_117.initGuildFightJoinRequestMessage(_loc_116.taxCollectorId);
                    ConnectionsHandler.getConnection().send(_loc_117);
                    return true;
                }
                case param1 is GuildFightJoinRequestAction:
                {
                    _loc_118 = param1 as GuildFightTakePlaceRequestAction;
                    _loc_119 = new GuildFightTakePlaceRequestMessage();
                    _loc_119.initGuildFightTakePlaceRequestMessage(_loc_118.taxCollectorId, _loc_118.replacedCharacterId);
                    ConnectionsHandler.getConnection().send(_loc_119);
                    return true;
                }
                case param1 is GuildFightTakePlaceRequestAction:
                {
                    _loc_120 = param1 as GuildFightLeaveRequestAction;
                    this._autoLeaveHelpers = false;
                    if (_loc_120.warning)
                    {
                        for each (_loc_156 in TaxCollectorsManager.getInstance().taxCollectors)
                        {
                            
                            if (_loc_156.state == 1)
                            {
                                _loc_157 = TaxCollectorsManager.getInstance().taxCollectorsFighters[_loc_156.uniqueId];
                                for each (_loc_158 in _loc_157.allyCharactersInformations)
                                {
                                    
                                    if (_loc_158.playerCharactersInformations.id == _loc_120.characterId)
                                    {
                                        this._autoLeaveHelpers = true;
                                        _loc_121 = new GuildFightLeaveRequestMessage();
                                        _loc_121.initGuildFightLeaveRequestMessage(_loc_156.uniqueId, _loc_120.characterId);
                                        ConnectionsHandler.getConnection().send(_loc_121);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        _loc_121 = new GuildFightLeaveRequestMessage();
                        _loc_121.initGuildFightLeaveRequestMessage(_loc_120.taxCollectorId, _loc_120.characterId);
                        ConnectionsHandler.getConnection().send(_loc_121);
                    }
                    return true;
                }
                case param1 is GuildFightLeaveRequestAction:
                {
                    _loc_122 = param1 as TaxCollectorHireRequestAction;
                    if (TaxCollectorsManager.getInstance().taxCollectorHireCost <= PlayedCharacterManager.getInstance().characteristics.kamas)
                    {
                        _loc_159 = new TaxCollectorHireRequestMessage();
                        _loc_159.initTaxCollectorHireRequestMessage();
                        ConnectionsHandler.getConnection().send(_loc_159);
                    }
                    else
                    {
                        KernelEventsManager.getInstance().processCallback(ChatHookList.TextInformation, I18n.getUiText("ui.popup.not_enough_rich"), ChatFrame.RED_CHANNEL_ID, TimeManager.getInstance().getTimestamp());
                    }
                    return true;
                }
                case param1 is TaxCollectorHireRequestAction:
                {
                    if (this._guildHousesList)
                    {
                        _loc_160 = param1 as GuildHouseUpdateInformationMessage;
                        _loc_161 = false;
                        for each (_loc_162 in this._guildHouses)
                        {
                            
                            if (_loc_162.houseId == _loc_160.housesInformations.houseId)
                            {
                                _loc_162.update(_loc_160.housesInformations);
                                _loc_161 = true;
                            }
                            KernelEventsManager.getInstance().processCallback(SocialHookList.GuildHousesUpdate);
                        }
                        if (true)
                        {
                            _loc_163 = GuildHouseWrapper.create(_loc_160.housesInformations);
                            this._guildHouses.push(_loc_163);
                            KernelEventsManager.getInstance().processCallback(SocialHookList.GuildHouseAdd, _loc_163);
                        }
                        this._guildHousesListUpdate = true;
                    }
                    return true;
                }
                case param1 is GuildHouseUpdateInformationMessage:
                {
                    if (this._guildHousesList)
                    {
                        _loc_164 = param1 as GuildHouseRemoveMessage;
                        _loc_165 = false;
                        _loc_166 = 0;
                        while (_loc_166 < this._guildHouses.length)
                        {
                            
                            if (this._guildHouses[_loc_166].houseId == _loc_164.houseId)
                            {
                                this._guildHouses.splice(_loc_166, 1);
                                break;
                            }
                            _loc_166++;
                        }
                        this._guildHousesListUpdate = true;
                        KernelEventsManager.getInstance().processCallback(SocialHookList.GuildHouseRemoved, _loc_164.houseId);
                    }
                    return true;
                }
                case param1 is GuildHouseRemoveMessage:
                {
                    _loc_123 = param1 as CharacterReportAction;
                    _loc_124 = new CharacterReportMessage();
                    _loc_124.initCharacterReportMessage(_loc_123.reportedId, _loc_123.reason);
                    ConnectionsHandler.getConnection().send(_loc_124);
                    return true;
                }
                case param1 is CharacterReportAction:
                {
                    _loc_125 = param1 as ChatReportAction;
                    _loc_126 = new ChatMessageReportMessage();
                    _loc_127 = Kernel.getWorker().getFrame(ChatFrame) as ChatFrame;
                    _loc_128 = _loc_127.getTimestampServerByRealTimestamp(_loc_125.timestamp);
                    _loc_126.initChatMessageReportMessage(_loc_125.name, _loc_125.message, _loc_128, _loc_125.channel, _loc_125.fingerprint, _loc_125.reason);
                    ConnectionsHandler.getConnection().send(_loc_126);
                    return true;
                }
                default:
                {
                    break;
                }
            }
            return false;
        }// end function

        public function pulled() : Boolean
        {
            TaxCollectorsManager.getInstance().destroy();
            return true;
        }// end function

        public function isIgnored(param1:String, param2:int = 0) : Boolean
        {
            var _loc_4:IgnoredWrapper;
            var _loc_3:* = AccountManager.getInstance().getAccountName(param1);
            for each (_loc_4 in this._ignoredList)
            {
                
                if (param2 != 0 && _loc_4.id == param2 || _loc_3 && _loc_4.name.toLowerCase() == _loc_3.toLowerCase())
                {
                    return true;
                }
            }
            return false;
        }// end function

        public function isFriend(param1:String) : Boolean
        {
            var _loc_4:FriendWrapper;
            var _loc_2:* = this._friendsList.length;
            var _loc_3:int;
            while (_loc_3 < _loc_2)
            {
                
                _loc_4 = this._friendsList[_loc_3];
                if (_loc_4.playerName == param1)
                {
                    return true;
                }
                _loc_3++;
            }
            return false;
        }// end function

        public function isEnemy(param1:String) : Boolean
        {
            var _loc_4:EnemyWrapper;
            var _loc_2:* = this._enemiesList.length;
            var _loc_3:int;
            while (_loc_3 < _loc_2)
            {
                
                _loc_4 = this._enemiesList[_loc_3];
                if (_loc_4.playerName == param1)
                {
                    return true;
                }
                _loc_3++;
            }
            return false;
        }// end function

    }
}
