package com.ankamagames.dofus.logic.game.common.misc
{
    import __AS3__.vec.*;
    import com.ankamagames.berilia.types.data.*;

    public class HookLock extends Object implements IHookLock
    {
        private var _hooks:Vector.<HookDef>;

        public function HookLock()
        {
            this._hooks = new Vector.<HookDef>;
            return;
        }// end function

        public function addHook(param1:Hook, param2:Array) : void
        {
            var _loc_4:HookDef;
            var _loc_3:* = new HookDef(param1, param2);
            for each (_loc_4 in this._hooks)
            {
                
                if (_loc_3.isEqual(_loc_4))
                {
                    return;
                }
            }
            this._hooks.push(_loc_3);
            return;
        }// end function

        public function release() : void
        {
            var _loc_1:HookDef;
            for each (_loc_1 in this._hooks)
            {
                
                _loc_1.run();
            }
            this._hooks.splice(0, this._hooks.length);
            return;
        }// end function

    }
}
