package com.ankamagames.dofus.misc.utils.mapeditor
{
    import com.ankamagames.atouin.data.map.*;
    import com.ankamagames.atouin.managers.*;
    import com.ankamagames.atouin.resources.adapters.*;
    import com.ankamagames.atouin.types.events.*;
    import com.ankamagames.dofus.*;
    import com.ankamagames.dofus.datacenter.npcs.*;
    import com.ankamagames.dofus.datacenter.world.*;
    import com.ankamagames.dofus.kernel.*;
    import com.ankamagames.dofus.misc.*;
    import com.ankamagames.dofus.network.enums.*;
    import com.ankamagames.dofus.network.messages.game.context.roleplay.*;
    import com.ankamagames.dofus.network.types.game.context.*;
    import com.ankamagames.dofus.network.types.game.context.fight.*;
    import com.ankamagames.dofus.network.types.game.context.roleplay.*;
    import com.ankamagames.dofus.network.types.game.house.*;
    import com.ankamagames.dofus.network.types.game.interactive.*;
    import com.ankamagames.jerakine.logger.*;
    import com.ankamagames.jerakine.resources.*;
    import com.ankamagames.jerakine.types.*;
    import com.ankamagames.jerakine.utils.display.*;
    import com.ankamagames.jerakine.utils.misc.*;
    import com.ankamagames.tiphon.types.look.*;
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;

    public class MapEditorManager extends Object
    {
        private var _mapEditorConnector:MapEditorConnector;
        private var _currentPopup:Sprite;
        private var _currentNpcInfos:MapComplementaryInformationsDataMessage;
        private var _currentRenderId:uint;
        private var _lastRenderedId:uint;
        static const _log:Logger = Log.getLogger(getQualifiedClassName(this));
        private static const COLOR_CONNECTED:uint = 12579390;
        private static const COLOR_CLOSE:uint = 15892246;

        public function MapEditorManager()
        {
            if (BuildInfos.BUILD_TYPE < BuildTypeEnum.INTERNAL)
            {
                return;
            }
            this.init();
            return;
        }// end function

        private function init() : void
        {
            this._mapEditorConnector = new MapEditorConnector();
            this._mapEditorConnector.addEventListener(MapEditorDataEvent.NEW_DATA, this.onNewData);
            this._mapEditorConnector.addEventListener(Event.CONNECT, this.onConnect);
            this._mapEditorConnector.addEventListener(Event.CLOSE, this.onClose);
            this._mapEditorConnector.addEventListener(ProgressEvent.SOCKET_DATA, this.onDataProgress);
            return;
        }// end function

        private function displayPopup(param1:String, param2:uint, param3:Boolean = false) : void
        {
            if (this._currentPopup)
            {
                this.closePopup();
            }
            this._currentPopup = new Sprite();
            this._currentPopup.mouseChildren = false;
            this._currentPopup.addEventListener(MouseEvent.CLICK, this.closePopup);
            var _loc_4:* = new TextField();
            new TextField().defaultTextFormat = new TextFormat("Verdana", 12, 0, true);
            _loc_4.autoSize = TextFieldAutoSize.LEFT;
            _loc_4.height = 30;
            _loc_4.text = param1;
            this._currentPopup.addChild(_loc_4);
            this._currentPopup.graphics.beginFill(param2);
            this._currentPopup.graphics.lineStyle(1, 6710886);
            this._currentPopup.graphics.drawRect(-5, -5, 20 + _loc_4.textWidth, _loc_4.height + 10);
            this._currentPopup.graphics.endFill();
            StageShareManager.stage.addChild(this._currentPopup);
            this._currentPopup.x = (StageShareManager.startWidth - _loc_4.textWidth) / 2;
            this._currentPopup.y = 10;
            if (param3)
            {
                setTimeout(this.closePopup, 5000, null, this._currentPopup);
            }
            return;
        }// end function

        private function closePopup(param1:Event = null, param2:Sprite = null) : void
        {
            if (!param2)
            {
                param2 = this._currentPopup;
            }
            if (!param2)
            {
                return;
            }
            param2.removeEventListener(MouseEvent.CLICK, this.closePopup);
            if (param2.parent)
            {
                param2.parent.removeChild(param2);
            }
            if (param2 == this._currentPopup)
            {
                this._currentPopup = null;
            }
            return;
        }// end function

        private function onNewData(param1:MapEditorDataEvent) : void
        {
            var _loc_2:MapsAdapter;
            var _loc_3:ElementsAdapter;
            var _loc_4:uint;
            var _loc_5:uint;
            var _loc_6:Vector.<GameRolePlayActorInformations>;
            var _loc_7:MapComplementaryInformationsDataMessage;
            var _loc_8:SubArea;
            var _loc_9:uint;
            var _loc_10:GameRolePlayNpcInformations;
            var _loc_11:int;
            var _loc_12:int;
            var _loc_13:int;
            var _loc_14:EntityDispositionInformations;
            this.displayPopup("Donn�es provenant de l\'�diteur " + param1.data.type, COLOR_CONNECTED);
            switch(param1.data.type)
            {
                case MapEditorMessage.MESSAGE_TYPE_DLM:
                {
                    this.displayPopup("Rendu d\'une map provenant de l\'�diteur", COLOR_CONNECTED);
                    _log.info("Rendu d\'une map provenant de l\'�diteur");
                    _loc_2 = new MapsAdapter();
                    _loc_2.loadFromData(new Uri(), param1.data.data, new ResourceObserverWrapper(this.onDmlLoaded), false);
                    break;
                }
                case MapEditorMessage.MESSAGE_TYPE_ELE:
                {
                    this.displayPopup("Donn�e sur les �l�ments", COLOR_CONNECTED);
                    _log.info("Parsing du fichier .ele provenant de l\'�diteur");
                    _loc_3 = new ElementsAdapter();
                    _loc_3.loadFromData(new Uri(), param1.data.data, new ResourceObserverWrapper(), false);
                    break;
                }
                case MapEditorMessage.MESSAGE_TYPE_NPC:
                {
                    _loc_4 = param1.data.data.readInt();
                    _loc_5 = param1.data.data.readInt();
                    _loc_6 = new Vector.<GameRolePlayActorInformations>;
                    _loc_9 = 0;
                    while (_loc_9++ < _loc_5)
                    {
                        
                        _loc_10 = new GameRolePlayNpcInformations();
                        _loc_11 = param1.data.data.readShort();
                        _loc_12 = param1.data.data.readInt();
                        _loc_13 = param1.data.data.readByte();
                        _loc_14 = new EntityDispositionInformations();
                        _loc_14.initEntityDispositionInformations(_loc_11, _loc_13);
                        _loc_10.initGameRolePlayNpcInformations(_loc_12, EntityLookAdapter.toNetwork(TiphonEntityLook.fromString(Npc.getNpcById(_loc_12).look)), _loc_14, _loc_12);
                        _loc_6.push(_loc_10);
                    }
                    _loc_7 = new MapComplementaryInformationsDataMessage();
                    _loc_8 = SubArea.getSubAreaByMapId(_loc_4);
                    _loc_7.initMapComplementaryInformationsDataMessage(_loc_8 ? (_loc_8.id) : (0), _loc_4, 0, new Vector.<HouseInformations>, _loc_6, new Vector.<InteractiveElement>, new Vector.<StatedElement>, new Vector.<MapObstacle>, new Vector.<FightCommonInformations>);
                    if (this._lastRenderedId == MapDisplayManager.getInstance().currentRenderId)
                    {
                        Kernel.getWorker().process(_loc_7);
                        this._currentNpcInfos = null;
                    }
                    else
                    {
                        this._currentNpcInfos = _loc_7;
                    }
                    break;
                }
                case MapEditorMessage.MESSAGE_TYPE_HELLO:
                {
                    this.displayPopup("Hello Alea", COLOR_CONNECTED);
                    break;
                }
                default:
                {
                    break;
                }
            }
            return;
        }// end function

        private function onMapRenderEnd(param1:RenderMapEvent) : void
        {
            this._lastRenderedId = param1.renderId;
            if (this._currentNpcInfos && param1.renderId == this._currentRenderId)
            {
                Kernel.getWorker().process(this._currentNpcInfos);
                this._currentNpcInfos = null;
            }
            this.displayPopup("Taille des picto : " + StringUtils.formateIntToString(uint(MapDisplayManager.getInstance().renderer.gfxMemorySize / 1024)) + " Ko", COLOR_CONNECTED);
            return;
        }// end function

        private function onDmlLoaded(param1:Uri, param2:uint, param3) : void
        {
            MapDisplayManager.getInstance().renderer.useDefautState = true;
            var _loc_4:* = new Map();
            new Map().fromRaw(param3);
            this._currentRenderId = MapDisplayManager.getInstance().fromMap(_loc_4);
            return;
        }// end function

        private function onConnect(param1:Event) : void
        {
            this.displayPopup("Connect� � l\'�diteur", COLOR_CONNECTED);
            _log.info("Connect� � l\'�diteur de map");
            MapDisplayManager.getInstance().renderer.addEventListener(RenderMapEvent.MAP_RENDER_END, this.onMapRenderEnd);
            return;
        }// end function

        private function onDataProgress(param1:Event) : void
        {
            this.displayPopup("R�ception de donn�es", COLOR_CONNECTED);
            _log.info("R�ception de donn�es");
            return;
        }// end function

        private function onClose(param1:Event) : void
        {
            this.displayPopup("Connexion � l\'�diteur de map perdue", COLOR_CLOSE);
            _log.info("Connexion � l\'�diteur de map perdue");
            return;
        }// end function

    }
}
