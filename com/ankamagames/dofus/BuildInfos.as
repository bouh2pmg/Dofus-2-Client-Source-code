package com.ankamagames.dofus
{
    import com.ankamagames.dofus.network.enums.*;
    import com.ankamagames.jerakine.types.*;

    final public class BuildInfos extends Object
    {
        public static var BUILD_VERSION:Version = new Version(2, 8, 3);
        public static var BUILD_TYPE:uint = 0;
        public static var BUILD_REVISION:int = 35100;
        public static var BUILD_PATCH:int = 0;
        public static const BUILD_DATE:String = "Oct 24, 2012 - 16:15:05 CEST";

        public function BuildInfos()
        {
            return;
        }// end function

    }
}
