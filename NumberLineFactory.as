package 
{
    import flash.text.engine.*;
    import flashx.textLayout.elements.*;
    import flashx.textLayout.factory.*;
    import flashx.textLayout.formats.*;

    private class NumberLineFactory extends StringTextLineFactory
    {
        private var _listStylePosition:String;
        private var _markerFormat:ITextLayoutFormat;
        private var _backgroundManager:BackgroundManager;

        private function NumberLineFactory()
        {
            return;
        }// end function

        public function get listStylePosition() : String
        {
            return this._listStylePosition;
        }// end function

        public function set listStylePosition(param1:String) : void
        {
            this._listStylePosition = param1;
            return;
        }// end function

        public function get markerFormat() : ITextLayoutFormat
        {
            return this._markerFormat;
        }// end function

        public function set markerFormat(param1:ITextLayoutFormat) : void
        {
            this._markerFormat = param1;
            spanFormat = param1;
            return;
        }// end function

        public function get backgroundManager() : BackgroundManager
        {
            return this._backgroundManager;
        }// end function

        public function clearBackgroundManager() : void
        {
            this._backgroundManager = null;
            return;
        }// end function

        override protected function callbackWithTextLines(param1:Function, param2:Number, param3:Number) : void
        {
            var _loc_4:TextLine;
            var _loc_5:TextBlock;
            for each (_loc_4 in _factoryComposer._lines)
            {
                
                _loc_4.userData = new NumberLineUserData(this.listStylePosition, calculateInsideNumberLineWidth(_loc_4, textFlowFormat.blockProgression), this._markerFormat, paragraphFormat.direction);
                _loc_5 = _loc_4.textBlock;
                if (_loc_5)
                {
                    _loc_5.releaseLines(_loc_5.firstLine, _loc_5.lastLine);
                }
                _loc_4.x = _loc_4.x + param2;
                _loc_4.y = _loc_4.y + param3;
                _loc_4.validity = TextLineValidity.STATIC;
                this.param1(_loc_4);
            }
            return;
        }// end function

        override function processBackgroundColors(param1:TextFlow, param2:Function, param3:Number, param4:Number, param5:Number, param6:Number)
        {
            this._backgroundManager = param1.backgroundManager;
            param1.clearBackgroundManager();
            return;
        }// end function

        static function calculateInsideNumberLineWidth(param1:TextLine, param2:String) : Number
        {
            var _loc_6:Rectangle;
            var _loc_3:* = Number.MAX_VALUE;
            var _loc_4:* = Number.MIN_VALUE;
            var _loc_5:int;
            if (param2 == BlockProgression.TB)
            {
                while (_loc_5 < param1.atomCount)
                {
                    
                    if (param1.getAtomTextBlockBeginIndex(_loc_5) != param1.rawTextLength--)
                    {
                        _loc_6 = param1.getAtomBounds(_loc_5);
                        _loc_3 = Math.min(_loc_3, _loc_6.x);
                        _loc_4 = Math.max(_loc_4, _loc_6.right);
                    }
                    _loc_5++;
                }
            }
            else
            {
                while (_loc_5 < param1.atomCount)
                {
                    
                    if (param1.getAtomTextBlockBeginIndex(_loc_5) != param1.rawTextLength--)
                    {
                        _loc_6 = param1.getAtomBounds(_loc_5);
                        _loc_3 = Math.min(_loc_3, _loc_6.top);
                        _loc_4 = Math.max(_loc_4, _loc_6.bottom);
                    }
                    _loc_5++;
                }
            }
            param1.flushAtomData();
            return _loc_4 > _loc_3 ? (_loc_4 - _loc_3) : (0);
        }// end function

    }
}
