package flashx.textLayout.elements
{

    final public class InlineGraphicElementStatus extends Object
    {
        public static const LOAD_PENDING:String = "loadPending";
        public static const LOADING:String = "loading";
        public static const SIZE_PENDING:String = "sizePending";
        public static const READY:String = "ready";
        public static const ERROR:String = "error";

        public function InlineGraphicElementStatus()
        {
            return;
        }// end function

    }
}
