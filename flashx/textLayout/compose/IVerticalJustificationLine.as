package flashx.textLayout.compose
{

    public interface IVerticalJustificationLine
    {

        public function IVerticalJustificationLine();

        function get x() : Number;

        function set x(param1:Number) : void;

        function get y() : Number;

        function set y(param1:Number) : void;

        function get ascent() : Number;

        function get descent() : Number;

        function get height() : Number;

    }
}
