package flashx.textLayout.formats
{

    public interface ITabStopFormat
    {

        public function ITabStopFormat();

        function getStyle(param1:String);

        function get position();

        function get alignment();

        function get decimalAlignmentToken();

    }
}
