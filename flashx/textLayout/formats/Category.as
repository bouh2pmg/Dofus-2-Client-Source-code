package flashx.textLayout.formats
{

    final public class Category extends Object
    {
        public static const PARAGRAPH:String = "Paragraph";
        public static const CONTAINER:String = "Container";
        public static const CHARACTER:String = "Character";
        public static const LIST:String = "List";
        public static const TABSTOP:String = "TabStop";
        public static const STYLE:String = "Style";

        public function Category()
        {
            return;
        }// end function

    }
}
