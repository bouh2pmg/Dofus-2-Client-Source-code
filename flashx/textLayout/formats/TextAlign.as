package flashx.textLayout.formats
{

    final public class TextAlign extends Object
    {
        public static const START:String = "start";
        public static const END:String = "end";
        public static const LEFT:String = "left";
        public static const RIGHT:String = "right";
        public static const CENTER:String = "center";
        public static const JUSTIFY:String = "justify";

        public function TextAlign()
        {
            return;
        }// end function

    }
}
