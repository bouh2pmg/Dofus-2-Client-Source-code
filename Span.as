package 
{

    private class Span extends Object
    {
        public var x:Number;
        public var ymin:Number;
        public var ymax:Number;

        private function Span(param1:Number, param2:Number, param3:Number)
        {
            this.x = param1;
            this.ymin = param2;
            this.ymax = param3;
            return;
        }// end function

        public function overlapsInY(param1:Number, param2:Number) : Boolean
        {
            if (param2 >= this.ymin)
            {
            }
            return param1 < this.ymax;
        }// end function

        public function equals(param1:Number, param2:Number, param3:Number) : Boolean
        {
            if (param1 == this.x && param2 == this.ymin)
            {
            }
            return param3 == this.ymax;
        }// end function

    }
}
