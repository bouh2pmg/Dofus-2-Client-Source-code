package 
{
    import flashx.textLayout.compose.*;
    import flashx.textLayout.container.*;
    import flashx.textLayout.elements.*;
    import flashx.textLayout.formats.*;

    private class TB_VJHelper extends Object implements IVerticalAdjustmentHelper
    {
        private var _textFrame:ContainerController;
        private var adj:Number;

        private function TB_VJHelper(param1:ContainerController) : void
        {
            this._textFrame = param1;
            return;
        }// end function

        public function getBottom(param1:IVerticalJustificationLine, param2:ContainerController, param3:int, param4:int) : Number
        {
            var _loc_7:FloatCompositionData;
            var _loc_8:InlineGraphicElement;
            var _loc_5:* = this.getBaseline(param1) + param1.descent;
            var _loc_6:* = param3;
            while (_loc_6 < param4)
            {
                
                _loc_7 = param2.getFloatAt(_loc_6);
                if (_loc_7.floatType != Float.NONE)
                {
                    _loc_8 = param2.rootElement.findLeaf(_loc_7.absolutePosition) as InlineGraphicElement;
                    _loc_5 = Math.max(_loc_5, _loc_7.y + _loc_8.elementHeightWithMarginsAndPadding());
                }
                _loc_6++;
            }
            return _loc_5;
        }// end function

        public function getBottomOfLine(param1:IVerticalJustificationLine) : Number
        {
            return this.getBaseline(param1) + param1.descent;
        }// end function

        private function getBaseline(param1:IVerticalJustificationLine) : Number
        {
            if (param1 is TextFlowLine)
            {
                return param1.y + param1.ascent;
            }
            return param1.y;
        }// end function

        private function setBaseline(param1:IVerticalJustificationLine, param2:Number) : void
        {
            if (param1 is TextFlowLine)
            {
                param1.y = param2 - param1.ascent;
            }
            else
            {
                param1.y = param2;
            }
            return;
        }// end function

        public function computeMiddleAdjustment(param1:Number) : Number
        {
            var _loc_2:* = this._textFrame.compositionHeight - Number(this._textFrame.getTotalPaddingBottom());
            this.adj = (_loc_2 - param1) / 2;
            if (this.adj < 0)
            {
                this.adj = 0;
            }
            return this.adj;
        }// end function

        public function computeBottomAdjustment(param1:Number) : Number
        {
            var _loc_2:* = this._textFrame.compositionHeight - Number(this._textFrame.getTotalPaddingBottom());
            this.adj = _loc_2 - param1;
            if (this.adj < 0)
            {
                this.adj = 0;
            }
            return this.adj;
        }// end function

        public function applyAdjustment(param1:IVerticalJustificationLine) : void
        {
            param1.y = param1.y + this.adj;
            return;
        }// end function

        public function applyAdjustmentToFloat(param1:FloatCompositionData) : void
        {
            param1.y = param1.y + this.adj;
            return;
        }// end function

        public function computeJustifyAdjustment(param1:Array, param2:int, param3:int) : Number
        {
            this.adj = 0;
            if (param3 == 1)
            {
                return 0;
            }
            var _loc_4:* = param1[param2];
            var _loc_5:* = this.getBaseline(_loc_4);
            var _loc_6:* = param1[(param2 + param3)--];
            var _loc_7:* = this._textFrame.compositionHeight - Number(this._textFrame.getTotalPaddingBottom());
            var _loc_8:* = this._textFrame.compositionHeight - Number(this._textFrame.getTotalPaddingBottom()) - this.getBottomOfLine(_loc_6);
            if (this._textFrame.compositionHeight - Number(this._textFrame.getTotalPaddingBottom()) - this.getBottomOfLine(_loc_6) < 0)
            {
                return 0;
            }
            var _loc_9:* = this.getBaseline(_loc_6);
            this.adj = _loc_8 / (_loc_9 - _loc_5);
            return this.adj;
        }// end function

        public function applyJustifyAdjustment(param1:Array, param2:int, param3:int) : void
        {
            var _loc_7:IVerticalJustificationLine;
            var _loc_8:Number;
            var _loc_9:Number;
            if (param3 == 1 || this.adj == 0)
            {
                return;
            }
            var _loc_4:* = param1[param2];
            var _loc_5:* = this.getBaseline(_loc_4);
            var _loc_6:* = this.getBaseline(_loc_4);
            var _loc_10:int;
            while (_loc_10 < param3)
            {
                
                _loc_7 = param1[_loc_10 + param2];
                _loc_9 = this.getBaseline(_loc_7);
                _loc_8 = _loc_5 + (_loc_9 - _loc_6) * (1 + this.adj);
                this.setBaseline(_loc_7, _loc_8);
                _loc_6 = _loc_9;
                _loc_5 = _loc_8;
                _loc_10++;
            }
            return;
        }// end function

    }
}
