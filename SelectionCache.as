package 
{
    import flash.geom.*;

    final private class SelectionCache extends Object
    {
        private var _begIdx:int = -1;
        private var _endIdx:int = -1;
        private var _selectionBlocks:Array = null;

        private function SelectionCache()
        {
            return;
        }// end function

        public function get begIdx() : int
        {
            return this._begIdx;
        }// end function

        public function set begIdx(param1:int) : void
        {
            this._begIdx = param1;
            return;
        }// end function

        public function get endIdx() : int
        {
            return this._endIdx;
        }// end function

        public function set endIdx(param1:int) : void
        {
            this._endIdx = param1;
            return;
        }// end function

        public function pushSelectionBlock(param1:Rectangle) : void
        {
            if (!this._selectionBlocks)
            {
                this._selectionBlocks = new Array();
            }
            this._selectionBlocks.push(param1.clone());
            return;
        }// end function

        public function get selectionBlocks() : Array
        {
            return this._selectionBlocks;
        }// end function

        public function clear() : void
        {
            this._selectionBlocks = null;
            this._begIdx = -1;
            this._endIdx = -1;
            return;
        }// end function

    }
}
