package 
{
    import flashx.textLayout.elements.*;
    import flashx.textLayout.formats.*;

    private class NumberLineUserData extends Object
    {
        public var listStylePosition:String;
        public var insideLineWidth:Number;
        public var spanFormat:ITextLayoutFormat;
        public var paragraphDirection:String;
        public var listEndIndent:Number;
        public var backgroundManager:BackgroundManager;

        private function NumberLineUserData(param1:String, param2:Number, param3:ITextLayoutFormat, param4:String)
        {
            this.listStylePosition = param1;
            this.insideLineWidth = param2;
            this.spanFormat = param3;
            this.paragraphDirection = param4;
            return;
        }// end function

    }
}
